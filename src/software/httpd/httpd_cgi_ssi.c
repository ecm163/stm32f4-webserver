/**
  ******************************************************************************
  * @file    httpd_cg_ssi.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    31-October-2011
  * @brief   Webserver SSI and CGI handlers
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/


#include "lwip/debug.h"
#include "httpd.h"
#include "lwip/tcp.h"
#include "fs.h"
#include "main.h"
#include "stm32f4_discovery.h"
#include "hardware/rtc/rtc.h"
#include "software/templog/templog.h"
#include <string.h>
#include <stdlib.h>

extern struct sensor_t measdata[MAX_SENSORS];
extern uint32_t rfm12_crc_erros, rfm12_pack_recv, rfm12_pack_send;
extern uint32_t net_pack_send, net_pack_recv, net_bytes_send, net_bytes_recv;

tSSIHandler ADC_Page_SSI_Handler;

#ifdef INCLUDE_HTTPD_SSI_PARAMS
static int SSIHandler(int iIndex, char *pcInsert, int iInsertLen,
                pSSIParam *params);
#else
static int SSIHandler(int iIndex, char *pcInsert, int iInsertLen
#if LWIP_HTTPD_SSI_MULTIPART
                             , u16_t current_tag_part, u16_t *next_tag_part
#endif /* LWIP_HTTPD_SSI_MULTIPART */
#if LWIP_HTTPD_FILE_STATE
                             , void *connection_state
#endif /* LWIP_HTTPD_FILE_STATE */
		);
#endif

//*****************************************************************************
//
// This array holds all the strings that are to be recognized as SSI tag
// names by the HTTPD server.  The server will call SSIHandler to request a
// replacement string whenever the pattern <!--#tagname--> (where tagname
// appears in the following array) is found in ".ssi", ".shtml" or ".shtm"
// files that it serves. Max size is MAX_TAG_NAME_LEN
//
//*****************************************************************************
static const char *g_pcConfigSSITags[] = {
				"Date", 		// SSI_INDEX_DATE
                "DateTime", 	// SSI_INDEX_DATETIME
                "Time", 		// SSI_INDEX_TIME
                "Uptime",        // SSI_INDEX_UPTIME
                "Sensors",        // SSI_INDEX_SENSORS
                "Stats"        // SSI_INDEX_SENSORS
};

//*****************************************************************************
//
//! The number of individual SSI tags that the HTTPD server can expect to
//! find in our configuration pages.
//
//*****************************************************************************
#define NUM_CONFIG_SSI_TAGS     (sizeof(g_pcConfigSSITags) / sizeof (char *))

//*****************************************************************************
//
// SSI tag indices for each entry in the g_pcSSITags array.
//
//*****************************************************************************
#define SSI_INDEX_DATE						(0)
#define SSI_INDEX_DATETIME					(1)
#define SSI_INDEX_TIME						(2)
#define SSI_INDEX_UPTIME					(3)
#define SSI_INDEX_SENSORS					(4)
#define SSI_INDEX_STATS						(5)


/* CGI handler for LED control */ 
const char * LEDS_CGI_Handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]);

/* Html request for "/leds.cgi" will start LEDS_CGI_Handler */
const tCGI LEDS_CGI={"/leds.cgi", LEDS_CGI_Handler};

/* Cgi call table, only one CGI used */
tCGI CGI_TAB[1];

//*****************************************************************************
//
// This function is called by the HTTP server whenever it encounters an SSI
// tag in a web page.  The iIndex parameter provides the index of the tag in
// the g_pcConfigSSITags array. This function writes the substitution text
// into the pcInsert array, writing no more than iInsertLen characters.
// max size of array: MAX_TAG_INSERT_LEN
//
//*****************************************************************************
#ifdef INCLUDE_HTTPD_SSI_PARAMS
static int SSIHandler(int iIndex, char *pcInsert, int iInsertLen,
                pSSIParam *params)
#else
static int
SSIHandler(int iIndex, char *pcInsert, int iInsertLen
#if LWIP_HTTPD_SSI_MULTIPART
                             , u16_t current_tag_part, u16_t *next_tag_part
#endif /* LWIP_HTTPD_SSI_MULTIPART */
#if LWIP_HTTPD_FILE_STATE
                             , void *connection_state
#endif /* LWIP_HTTPD_FILE_STATE */
                             )
#endif
{
	/*
#ifndef INCLUDE_HTTPD_SSI_PARAMS
        pSSIParam *params = NULL;
#endif
*/
#ifdef SSI_DEBUG
        printf("SSI HANDLER \n");
#endif
        //
        // Which SSI tag have we been passed?
        //
    	static uint8_t first = 1;
    	clock_datetime_t dt;

        switch (iIndex) {
        case SSI_INDEX_DATE:
        	clock_current_localtime(&dt);
        	snprintf(pcInsert, iInsertLen, "%02d.%02d.%04d", dt.day, dt.month, 1900+dt.year);
                break;
        case SSI_INDEX_DATETIME:
        	clock_current_localtime(&dt);
        	snprintf(pcInsert, iInsertLen, "%02d.%02d.%04d %02d:%02d:%02d", dt.day, dt.month, 1900+dt.year,
        			dt.hour, dt.min, dt.sec);
                break;
        case SSI_INDEX_TIME:
        	clock_current_localtime(&dt);
        	snprintf(pcInsert, iInsertLen, "%02d:%02d:%02d", dt.hour, dt.min, dt.sec);
                break;
        case SSI_INDEX_UPTIME:
        	snprintf(pcInsert, iInsertLen, "%lu", clock_get_uptime());
                break;
        case SSI_INDEX_SENSORS:
        	if(current_tag_part == 0) first = 1;
        	if(measdata[current_tag_part].sensortyp == SENSOR_DS1820)
        	{

        		struct value_ds18x20 *data = (struct value_ds18x20 *)measdata[current_tag_part].values;
        		snprintf(pcInsert, iInsertLen, "%c{\"date\":%lu,\"sensorid\":%i,\"sensortyp\":%i,\"temp\":%li.%04u,\"humi\":null,\"bat\":%u.%03u}",
        				(first > 0 ? ' ' : ','), data[measdata[current_tag_part].last_value_index].timestamp, current_tag_part+1, SENSOR_DS1820,
        				(data[measdata[current_tag_part].last_value_index].temp * 625L) / 10000,
        				abs((data[measdata[current_tag_part].last_value_index].temp * 625L) % 10000),
        				data[measdata[current_tag_part].last_value_index].battery / 1000, data[measdata[current_tag_part].last_value_index].battery % 1000);
        		if(first) first = 0;
        	} else if (measdata[current_tag_part].sensortyp == SENSOR_SHT11) {
        		struct value_sht11 *data = (struct value_sht11 *)measdata[current_tag_part].values;
        		snprintf(pcInsert, iInsertLen, "%c{\"date\":%lu,\"sensorid\":%i,\"sensortyp\":%i,\"temp\":%i.%02u,\"humi\":%u.%02u,\"bat\":%u.%03u}",
        				(first > 0 ? ' ' : ','), data[measdata[current_tag_part].last_value_index].timestamp, current_tag_part+1, SENSOR_SHT11,
        				data[measdata[current_tag_part].last_value_index].temp / 100,
        				abs(data[measdata[current_tag_part].last_value_index].temp % 100),
        				data[measdata[current_tag_part].last_value_index].humi / 100,
        				data[measdata[current_tag_part].last_value_index].humi % 100,
        				data[measdata[current_tag_part].last_value_index].battery / 1000, data[measdata[current_tag_part].last_value_index].battery % 1000);
        		if(first) first = 0;
        	} else if (measdata[current_tag_part].sensortyp == SENSOR_HP03S) {
        		struct value_hp03 *data = (struct value_hp03 *)measdata[current_tag_part].values;
        		snprintf(pcInsert, iInsertLen, "%c{\"date\":%lu,\"sensorid\":%i,\"sensortyp\":%i,\"temp\":%i.%u,\"humi\":%i.%u,\"bat\":%u.%03u}",
        				(first > 0 ? ' ' : ','), data[measdata[current_tag_part].last_value_index].timestamp, current_tag_part+1, SENSOR_HP03S,
        				data[measdata[current_tag_part].last_value_index].temp / 100,
        				abs(data[measdata[current_tag_part].last_value_index].temp % 100),
        				data[measdata[current_tag_part].last_value_index].pressure / 10,
        				data[measdata[current_tag_part].last_value_index].pressure % 10,
        				data[measdata[current_tag_part].last_value_index].battery / 1000, data[measdata[current_tag_part].last_value_index].battery % 1000);
        		if(first) first = 0;
        	} else {
                  snprintf(pcInsert, iInsertLen, " ");
                }
        	if(++current_tag_part < MAX_SENSORS)
        		*next_tag_part = current_tag_part;
                break;
        case SSI_INDEX_STATS:
        	switch(current_tag_part) {
        		case 0:
                	clock_current_localtime(&dt);
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Uhrzeit\",\"wert\":\"%02d.%02d.%04d %02d:%02d:%02d\",\"leaf\":true,\"iconCls\":\"task\"},",
        					dt.day, dt.month, 1900+dt.year, dt.hour, dt.min, dt.sec);
        			break;
        		case 1:
        			snprintf(pcInsert, iInsertLen, "{\"feld\": \"Uptime\",\"wert\":\"%lu:%02ld\",\"leaf\":true,\"iconCls\":\"task\"},", clock_get_uptime() / 3600, (clock_get_uptime()/60)%60);
        			break;
        		case 2:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Ethernet\",\"wert\":\"\",\"iconCls\":\"task-folder\",\"expanded\": true,\"children\":[");
        			break;
        		case 3:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Rx Pakete\",\"wert\":%u,\"leaf\":true,\"iconCls\":\"task\"},", lwip_stats.ip.recv);
        			break;
        		case 4:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Tx Pakete\",\"wert\":%u,\"leaf\":true,\"iconCls\":\"task\"},", lwip_stats.ip.xmit);
        			break;
        		case 5:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Rx Bytes\",\"wert\":%lu,\"leaf\":true,\"iconCls\":\"task\"},", net_bytes_recv);
        			break;
        		case 6:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Tx Bytes\",\"wert\":%lu,\"leaf\":true,\"iconCls\":\"task\"}]},", net_bytes_send);
        			break;
        		case 7:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"RFM12\",\"wert\":\"\",\"iconCls\":\"task-folder\",\"expanded\": true,\"children\":[");
        			break;
        		case 8:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Rx Pakete\",\"wert\":%lu,\"leaf\":true,\"iconCls\":\"task\"},", rfm12_pack_recv);
        			break;
        		case 9:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"Tx Pakete\",\"wert\":%lu,\"leaf\":true,\"iconCls\":\"task\"},", rfm12_pack_send);
        			break;
        		case 10:
        			snprintf(pcInsert, iInsertLen, "{\"feld\":\"CRC Fehler\",\"wert\":%lu,\"leaf\":true,\"iconCls\":\"task\"}]}", rfm12_crc_erros);
        			current_tag_part = 0xffff - 1;
        			break;
        	}
        	*next_tag_part = current_tag_part + 1;
            break;
        default:
                snprintf(pcInsert, iInsertLen, "??");
                break;
        }

        //
        // Tell the server how many characters our insert string contains.
        //
        return (strlen(pcInsert));
}


/**
  * @brief  CGI handler for LEDs control 
  */
const char * LEDS_CGI_Handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[])
{
  
  /* We have only one SSI handler iIndex = 0 */
  if (iIndex==0)
  {
//    /* All leds off */
//    STM_EVAL_LEDOff(LED1);
//    STM_EVAL_LEDOff(LED2);
//    STM_EVAL_LEDOff(LED3);
//    STM_EVAL_LEDOff(LED4);
//
//    /* Check cgi parameter : example GET /leds.cgi?led=2&led=4 */
//    for (i=0; i<iNumParams; i++)
//    {
//      /* check parameter "led" */
//      if (strcmp(pcParam[i] , "led")==0)
//      {
//        /* switch led1 ON if 1 */
//        if(strcmp(pcValue[i], "1") ==0)
//          STM_EVAL_LEDOn(LED1);
//
//        /* switch led2 ON if 2 */
//        else if(strcmp(pcValue[i], "2") ==0)
//          STM_EVAL_LEDOn(LED2);
//
//        /* switch led3 ON if 3 */
//        else if(strcmp(pcValue[i], "3") ==0)
//          STM_EVAL_LEDOn(LED3);
//
//        /* switch led4 ON if 4 */
//        else if(strcmp(pcValue[i], "4") ==0)
//          STM_EVAL_LEDOn(LED4);
//      }
//    }
  }
  /* uri to send after cgi call*/
  return "/STM32F4x7LED.html";  
}

/**
 * Initialize SSI handlers
 */
void httpd_ssi_init(void)
{  
  /* configure SSI handlers (ADC page SSI) */
	http_set_ssi_handler(SSIHandler, g_pcConfigSSITags, NUM_CONFIG_SSI_TAGS);
}

/**
 * Initialize CGI handlers
 */
void httpd_cgi_init(void)
{ 
  /* configure CGI handlers (LEDs control CGI) */
  CGI_TAB[0] = LEDS_CGI;
  http_set_cgi_handlers(CGI_TAB, 1);
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
