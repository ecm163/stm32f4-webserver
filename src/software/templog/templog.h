/*
 * templog.h
 *
 *  Created on: Feb 23, 2012
 *      Author: black
 */

#ifndef TEMPLOG_H_
#define TEMPLOG_H_

#define MAX_SENSORS 			10
#define MAX_VALUES_PER_SENSOR	30

#define SENSOR_DS1820			0x55
#define SENSOR_SHT11			0x66
#define SENSOR_HP03S			0x77

#define SENSOR_ID				0x01
#define SENSOR_TYP				SENSOR_SHT11
#define RFM12_ACK				0x12
#define RFM12_NACK				0x13

#define RFM12_REG_S				0x10
#define RFM12_REG_R				0x20
#define RFM12_MEAS				0x30

#define sensor_timer 11			// timer x 10 = sekunden

void templog_init(void);
void templog_perodic(void);
void templog_write_data(void);
void temp_local_meas(void);

struct sensor_t {
	uint8_t		sensortyp;
	uint8_t		aktiv;
	uint8_t		last_value_index;	// last active index in values
	void 		*values;
};
struct value_ds18x20 {
	int16_t 	temp;				// temperature  * 0.0625 = temp ... 215 = 13,4375�C
	uint16_t	battery;			// voltage of battery in 1/100V ... 3201 = 3,201V
	uint32_t	timestamp;			// timestamp of measure value
};
struct value_sht11 {
	int16_t 	temp;				// temperature in 1/100�C .. -1521 = -15,21�C
	uint16_t 	humi;				// humidity in 1/100 %  ... 5356 = 53.56% rel. hum
	uint16_t	battery;			// voltage of battery in 1/100V ... 3201 = 3,201V
	uint32_t	timestamp;			// timestamp of measure value
};
struct value_hp03 {
	int16_t 	temp;				// temperature in 1/100�C .. -1521 = -15,21�C
	uint16_t 	pressure;			// pressure  in 1/10 hpa %  ... 10054 = 1005,4 hpa
	uint16_t	battery;			// voltage of battery in 1/100V ... 3201 = 3,201V
	uint32_t	timestamp;			// timestamp of measure value
};

#endif /* TEMPLOG_H_ */
