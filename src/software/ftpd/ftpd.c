/*
 * Copyright (c) 2012 Dennis Pfenning
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * 02.01.2012	V0.01	start of development
 *
 * Author: Dennis Pfenning <webmaster@easy-clanpage.de>
 *
 */

#include "ftpd.h"

#if LWIP_TCP
int ftpd_user_online = 0;
//#define LWIP_PLATFORM_DIAG printf
#define LWIP_DEBUGF1(debug, message) //LWIP_PLATFORM_DIAG message;

extern char Lfname[_MAX_LFN+1];
static const char *month_table[12] =
{
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dez"
};

static err_t ftpd_poll(void *arg, struct tcp_pcb *pcb);
static err_t ftpd_data_poll(void *arg, struct tcp_pcb *pcb);
static void ftpd_close_conn(struct tcp_pcb *pcb, struct ftpd_msgstate *hs);
static void ftpd_close_data_conn(struct tcp_pcb *pcb, struct ftpd_datastate *hs);
static err_t ftpd_data_accept(void *arg, struct tcp_pcb *pcb, err_t err);
static void send_next_directory(struct ftpd_datastate *fsd, struct tcp_pcb *pcb, int shortlist);
static err_t ftpd_data_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err);
static err_t ftpd_data_sent(void *arg, struct tcp_pcb *pcb, u16_t len);
static void ftpd_data_err(void *arg, err_t err);
static void ftpd_send_file(struct ftpd_datastate *fsd, struct tcp_pcb *pcb);
static void ftpd_data_state_free(struct ftpd_datastate *hs);
static struct ftpd_datastate* ftpd_datastate_alloc(void);

static err_t send_msg(struct tcp_pcb *pcb, struct ftpd_msgstate *fsm, char *msg, ...)
{
	va_list arg;
	int len;

	va_start(arg, msg);
	vsprintf(fsm->buf, msg, arg);
	va_end(arg);
	strcat(fsm->buf, "\r\n");


	err_t err;
	len = strlen(fsm->buf);
	//LWIP_ASSERT("length != NULL", length != NULL);
	do {
		//LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd: Sending %d bytes\n", len));
		err = tcp_write(pcb, (char*)fsm->buf, len, 0);
		if (err == ERR_MEM) {
			if ((tcp_sndbuf(pcb) == 0) ||
					(pcb->snd_queuelen >= TCP_SND_QUEUELEN)) {
				/* no need to try smaller sizes */
				len = 1;
			} else {
				len /= 2;
			}
		}
	} while ((err == ERR_MEM) && (len > 1));
	tcp_output(pcb);
	return err;
}
static int check_login (struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(fsm->state == FTPD_PASS || fsm->state == FTPD_USER) {
		send_msg(pcb, fsm, msg503_user);
		return 0;
	} else {
		return 1;
	}
}
static void cmd_dele(const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb,fsm)) {
		FILINFO finfo;

		if (arg == NULL)
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (*arg == '\0')
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (f_stat(arg, &finfo))
		{
			send_msg(pcb, fsm, msg550);
			return;
		}
		if (!f_unlink(arg))
		{
			send_msg(pcb, fsm, msg250);
		}
		else
		{
			send_msg(pcb, fsm, msg550);
		}
	}
}
static void cmd_user (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	int len;
	len = strlen(arg);
	if(len > FTPD_MAX_USER_LEN) {
		len = FTPD_MAX_USER_LEN;
	}
	memcpy(fsm->username, arg, len);
	fsm->username[len] = '\0';
	fsm->state = FTPD_PASS;
	send_msg(pcb, fsm, msg331);
}
static void cmd_pass (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(fsm->state == FTPD_USER) {
		send_msg(pcb, fsm, msg503_user);
	} else if (fsm->state == FTPD_PASS) {
		if(!strcmp(arg, FTPD_PASSWORD) && !strcmp(fsm->username, FTPD_USERNAME)) {
			fsm->state = FTPD_IDLE;
			send_msg(pcb, fsm, msg230);
		} else {
			fsm->state = FTPD_QUIT;
			send_msg(pcb, fsm, msg530);
		}
	} else {

	}
}
static void cmd_port (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		int nr;
		unsigned pHi, pLo;
		unsigned ip[4];

		nr = sscanf(arg, "%u,%u,%u,%u,%u,%u", &(ip[0]), &(ip[1]), &(ip[2]), &(ip[3]), &pHi, &pLo);
		if (nr != 6)
		{
			send_msg(pcb, fsm, msg501);
		}
		else
		{
			IP4_ADDR(&fsm->dataip, (u8_t) ip[0], (u8_t) ip[1], (u8_t) ip[2], (u8_t) ip[3]);
			fsm->dataport = ((u32_t) pHi << 8) + (u32_t)pLo;
			send_msg(pcb, fsm, msg200);
		}
	}
}
static void cmd_quit (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	fsm->state = FTPD_QUIT;
	send_msg(pcb, fsm, msg221);
	ftpd_close_conn(pcb, fsm);
}
static void cmd_cwd (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		if(f_chdir(arg) == 0) {
			if(arg[0] == '/')
				strcpy(fsm->path, arg);
			else {
				if(fsm->path[strlen(fsm->path)-1] != '/')
					strcat(fsm->path, "/");
				strcat(fsm->path, arg);
			}
			send_msg(pcb, fsm, msg250);
		} else {
			fsm->path[0] = '/';
			fsm->path[1] = '0';
			send_msg(pcb, fsm, msg550);
		}
	}
}
static void cmd_cdup (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		char * pch;
		pch=strrchr(fsm->path,'/');
		fsm->path[(pch-fsm->path) ? (pch-fsm->path) : 1] = '\0';			/* Keep root path / */
		if (!f_chdir(fsm->path))
		{
			send_msg(pcb, fsm, msg250);
		}
		else
		{
			send_msg(pcb, fsm, msg550);
		}
	}
}
static void cmd_pwd (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		send_msg(pcb, fsm, msg257PWD, fsm->path);
	}
}
static err_t ftpd_dataconnected(void *arg, struct tcp_pcb *pcb, err_t err)
{

	struct ftpd_datastate *fsd = arg;
	if(check_login(pcb, fsd->msgfs)) {
		fsd->msgfs->datapcb = pcb;
		fsd->connected = 1;
		tcp_recv(pcb, ftpd_data_recv);
		tcp_sent(pcb, ftpd_data_sent);
		tcp_err(pcb, ftpd_data_err);
		switch (fsd->msgfs->state)
		{
		case FTPD_LIST:
			send_next_directory(fsd, pcb, 0);
			break;
		case FTPD_NLST:
			send_next_directory(fsd, pcb, 1);
			break;
		case FTPD_RETR:
			ftpd_send_file(fsd, pcb);
			break;
		default:
			break;
		}
		return ERR_OK;
	} else {
		return ERR_CLSD;
	}
}
static int open_dataconnection(struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if (fsm->passive)
	{
		fsm->passive = 0;
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: pasv\n"));
		return 0;
	}
	if (fsm->datafs != NULL)
	{
		ftpd_data_state_free(fsm->datafs);
		fsm->datafs = NULL;
	}
	fsm->datafs = ftpd_datastate_alloc();
	if (fsm->datafs == NULL)
	{
		LWIP_DEBUGF1(FTPD_DEBUG, ("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
		send_msg(pcb, fsm, msg451);
		return 1;
	}
	memset(fsm->datafs, 0, sizeof(struct ftpd_datastate));
	fsm->datafs->msgfs = fsm;
	fsm->datafs->msgpcb = pcb;

	int count = (TCP_SND_BUF / TCP_MSS) * tcp_mss(pcb);
	do {
		fsm->datafs->buf = (char*)mem_malloc((mem_size_t)count);
		if (fsm->datafs->buf != NULL) {
			fsm->datafs->buf_size = count;
			break;
		}
		count = count / 2;
	} while (count > 100);

	/* Did we get a send buffer? If not, return immediately. */
	if (fsm->datafs->buf == NULL) {
		LWIP_DEBUGF1(FTPD_DEBUG, ("No buff\n"));
		return -1;
	}

	fsm->datapcb = tcp_new();
	tcp_bind(fsm->datapcb, &pcb->local_ip, FTPD_DATA_PORT);
	tcp_arg(fsm->datapcb, fsm->datafs);
	if ( ERR_OK != tcp_connect(fsm->datapcb, &fsm->dataip, fsm->dataport, ftpd_dataconnected))
	{
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: ERROR open connection\n"));
		return -1;
	}
	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: Connection open %i.%i.%i.%i:%i\n", ip4_addr1(&fsm->dataip), ip4_addr2(&fsm->dataip), ip4_addr3(&fsm->dataip), ip4_addr4(&fsm->dataip), fsm->dataport));
	return 0;
}
static void cmd_list_common(const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm, int shortlist)
{
	if(check_login(pcb, fsm)) {
		if (open_dataconnection(pcb, fsm) != 0)
		{
			return;
		}
		if (shortlist != 0)
		{
			fsm->state = FTPD_NLST;
		}
		else
		{
			fsm->state = FTPD_LIST;
		}
		send_msg(pcb, fsm, msg150);
	}
}

static void cmd_nlst(const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	cmd_list_common(arg, pcb, fsm, 1);
}

static void cmd_list(const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	cmd_list_common(arg, pcb, fsm, 0);
}
static void cmd_retr (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		if (open_dataconnection(pcb, fsm) != 0)
		{
			return;
		}

		FRESULT rc;
		if(fsm->datafs->fil == NULL) {
			fsm->datafs->fil = mem_malloc(sizeof(FIL));
			if (fsm->datafs->fil == NULL)
			{
				LWIP_DEBUGF1(FTPD_DEBUG,  ("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
				return;
			}
		}

		rc = f_open(fsm->datafs->fil, arg, FA_READ);
		if (rc)
		{
			send_msg(pcb, fsm, msg550);
			return;
		}
		send_msg(pcb, fsm, msg150recv, arg, f_size(fsm->datafs->fil));
		fsm->datafs->filepos = 0;
		fsm->state = FTPD_RETR;
	}
}
static void cmd_stor (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		FRESULT rc;
		if(fsm->datafs->fil == NULL) {
			fsm->datafs->fil = mem_malloc(sizeof(FIL));
			if (fsm->datafs->fil == NULL)
			{
				LWIP_DEBUGF1(FTPD_DEBUG,  ("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
				return;
			}
		}
		rc = f_open(fsm->datafs->fil, arg, FA_WRITE | FA_OPEN_ALWAYS);
		if (rc)
		{
			send_msg(pcb, fsm, msg550);
			return;
		}
		send_msg(pcb, fsm, msg150stor, arg);
		if (open_dataconnection(pcb, fsm) != 0)
		{
			f_close(fsm->datafs->fil);
			mem_free(fsm->datafs->fil);
			fsm->datafs->fil = NULL;
			return;
		}
		fsm->datafs->filepos = 0;
		fsm->state = FTPD_STOR;
	}
}
static void cmd_noop (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		send_msg(pcb,fsm,msg200);
	}
}
static void cmd_syst (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		send_msg(pcb, fsm, msg214SYST, "UNIX");
	}
}
static void cmd_abrt (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	/* TODO Daten freigeben */
	tcp_arg(fsm->datapcb, NULL);
	tcp_sent(fsm->datapcb, NULL);
	tcp_recv(fsm->datapcb, NULL);
	tcp_abort(pcb);
	fsm->state = FTPD_IDLE;
}
static void cmd_type (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		send_msg(pcb, fsm, msg200);
	}
}
static void cmd_mode (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	send_msg(pcb, fsm, msg502);
}
static void cmd_rnfr (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb,fsm)) {
		if (arg == NULL)
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (*arg == '\0')
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (fsm->renamefrom)
		{
			mem_free(fsm->renamefrom);
			fsm->renamefrom = NULL;
		}
		fsm->renamefrom = mem_malloc(strlen(arg) + 1);
		if (fsm->renamefrom == NULL)
		{
			LWIP_DEBUGF1(FTPD_DEBUG,("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
			send_msg(pcb, fsm, msg451);
			return;
		}
		strcpy(fsm->renamefrom, arg);
		fsm->state = FTPD_RNFR;
		send_msg(pcb, fsm, msg350);
	}
}
static void cmd_rnto (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb,fsm)) {
		if (fsm->state != FTPD_RNFR)
		{
			send_msg(pcb, fsm, msg550_rnfr);
			return;
		}
		fsm->state = FTPD_IDLE;
		if (arg == NULL)
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (*arg == '\0')
		{
			send_msg(pcb, fsm, msg501);
			return;
		}
		if (!f_rename(fsm->renamefrom, arg))
		{
			send_msg(pcb, fsm, msg250);
		}
		else
		{
			send_msg(pcb, fsm, msg450);
		}
		mem_free(fsm->renamefrom);
		fsm->renamefrom = NULL;
	}
}
static void cmd_mkd (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if (arg == NULL)
	{
		send_msg(pcb, fsm, msg501);
		return;
	}
	if (*arg == '\0')
	{
		send_msg(pcb, fsm, msg501);
		return;
	}
	if (!f_mkdir(arg))
	{
		send_msg(pcb, fsm, msg257, arg);
	}
	else
	{
		send_msg(pcb, fsm, msg550);
	}
}
static void cmd_rmd (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	cmd_dele(arg,pcb,fsm);
}
static void cmd_pasv (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		static u16_t port = 4096;
		static u16_t start_port = 4096;
		struct tcp_pcb *temppcb;
		int count;

		if (fsm->datafs != NULL)
		{
			ftpd_data_state_free(fsm->datafs);
			fsm->datafs = NULL;
		}
		fsm->datafs = ftpd_datastate_alloc();

		if (fsm->datafs == NULL)
		{
			LWIP_DEBUGF1(FTPD_DEBUG, ("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
			send_msg(pcb, fsm, msg451);
			return;
		}

		memset(fsm->datafs, 0, sizeof(struct ftpd_datastate));
		if (fsm->datapcb != NULL)
		{
			tcp_close(fsm->datapcb);
		}
		fsm->datapcb = tcp_new();
		if (!fsm->datapcb)
		{
			ftpd_data_state_free(fsm->datafs);
			fsm->datafs = NULL;
			send_msg(pcb, fsm, msg451);
			return;
		}
		count = (TCP_SND_BUF / TCP_MSS) * tcp_mss(pcb);
		do {
			fsm->datafs->buf = (char*)mem_malloc((mem_size_t)count);
			if (fsm->datafs->buf != NULL) {
				fsm->datafs->buf_size = count;
				break;
			}
			count = count / 2;
		} while (count > 100);

		/* Did we get a send buffer? If not, return immediately. */
		if (fsm->datafs->buf == NULL) {
			LWIP_DEBUGF1(FTPD_DEBUG, ("No buff\n"));
			return;
		}

		start_port = port;

		while (1)
		{
			err_t err;

			if(++port > 0x4fff)
			{
				port = 4096;
			}

			fsm->dataport = port;
			err = tcp_bind(fsm->datapcb, &pcb->local_ip, fsm->dataport);
			if (err == ERR_OK)
			{
				break;
			}
			if (start_port == port)
			{
				err = ERR_CLSD;
			}
			if (err == ERR_USE)
			{
				continue;
			}
			if (err != ERR_OK)
			{
				ftpd_close_data_conn(fsm->datapcb, fsm->datafs);
				fsm->datapcb = NULL;
				fsm->datafs = NULL;
				return;
			}
		}

		temppcb = tcp_listen(fsm->datapcb);
		if (!temppcb)
		{
			printf(" tmppcb fails \r\n");
			ftpd_close_data_conn(fsm->datapcb, fsm->datafs);
			fsm->datapcb = NULL;
			fsm->datafs = NULL;
			return;
		}

		fsm->listenpcb = temppcb;

		fsm->passive = 1;
		fsm->datafs->connected = 0;
		fsm->datafs->msgfs = fsm;
		fsm->datafs->msgpcb = pcb;

		tcp_arg(fsm->listenpcb, fsm->datafs);
		tcp_accept(fsm->listenpcb, ftpd_data_accept);
		send_msg(pcb, fsm, msg227, ip4_addr1(&pcb->local_ip), ip4_addr2(&pcb->local_ip), ip4_addr3(&pcb->local_ip), ip4_addr4(&pcb->local_ip), (fsm->dataport >> 8) & 0xff, (fsm->dataport) & 0xff);
	}
}
static void cmd_size (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	if(check_login(pcb, fsm)) {
		FILINFO info;
		f_stat(arg, &info);
		send_msg(pcb,fsm, msg213, info.fsize);
	}
}
static void cmd_appe (const char *arg, struct tcp_pcb *pcb, struct ftpd_msgstate *fsm)
{
	/* TODO Error for example disk full */
	cmd_stor(arg, pcb, fsm);
	if(f_lseek(fsm->datafs->fil,f_size(fsm->datafs->fil)) != FR_OK) {
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd: APPE failed"));
	}
}
static struct ftpd_command ftpd_commands[] = {
		{"USER", cmd_user},
		{"PASS", cmd_pass},
		{"PORT", cmd_port},
		{"QUIT", cmd_quit},
		{"BYE",  cmd_quit},
		{"CWD",  cmd_cwd},
		{"CDUP", cmd_cdup},
		{"PWD",  cmd_pwd},
		{"XPWD", cmd_pwd},
		{"NLST", cmd_nlst},
		{"LIST", cmd_list},
		{"RETR", cmd_retr},
		{"STOR", cmd_stor},
		{"NOOP", cmd_noop},
		{"SYST", cmd_syst},
		{"ABOR", cmd_abrt},
		{"TYPE", cmd_type},
		{"MODE", cmd_mode},
		{"RNFR", cmd_rnfr},
		{"RNTO", cmd_rnto},
		{"MKD",  cmd_mkd},
		{"XMKD", cmd_mkd},
		{"RMD",  cmd_rmd},
		{"XRMD", cmd_rmd},
		{"DELE", cmd_dele},
		{"PASV", cmd_pasv},
		{"SIZE", cmd_size},
		{"APPE", cmd_appe},
		{NULL,NULL}
};


/** Free a struct http_state.
 * Also frees the file data if dynamic.
 */
static void ftpd_data_state_free(struct ftpd_datastate *hs)
{
	if (hs != NULL) {
		if(hs->msgfs->datapcb !=NULL) {
			tcp_close(hs->msgfs->datapcb);
			hs->msgfs->datapcb = NULL;
		}
		if(hs->msgfs->listenpcb !=NULL) {
			tcp_close(hs->msgfs->listenpcb);
			hs->msgfs->listenpcb = NULL;
		}
		if(hs->fil != NULL) {
			f_close(hs->fil);
			mem_free(hs->fil);
			hs->fil = NULL;
		}
		if(hs->dir != NULL) {
			mem_free(hs->dir);
			hs->dir = NULL;
		}
		if(hs->fno != NULL) {
			mem_free(hs->fno);
			hs->fno = NULL;
		}
		if (hs->buf != NULL) {
			mem_free(hs->buf);
			hs->buf = NULL;
		}
#if HTTPD_USE_MEM_POOL
		memp_free(MEMP_FTPD_DATA_STATE, hs);
#else /* HTTPD_USE_MEM_POOL */
		hs->msgfs->datafs = NULL;
		mem_free(hs);
		hs = NULL;
#endif /* HTTPD_USE_MEM_POOL */
	}
}
/** Free a struct http_state.
 * Also frees the file data if dynamic.
 */
static void ftpd_state_free(struct ftpd_msgstate *hs)
{
	if (hs != NULL) {
		if (hs->buf != NULL) {
			mem_free(hs->buf);
			hs->buf = NULL;
		}
		if(hs->renamefrom !=NULL) {
			mem_free(hs->renamefrom);
			hs->renamefrom = NULL;
		}
#if HTTPD_USE_MEM_POOL
		memp_free(MEMP_HTTPD_STATE, hs);
#else /* HTTPD_USE_MEM_POOL */
		mem_free(hs);
		hs = NULL;
#endif /* HTTPD_USE_MEM_POOL */
	}
	ftpd_user_online--;
}
/** Call tcp_write() in a loop trying smaller and smaller length
 *
 * @param pcb tcp_pcb to send
 * @param ptr Data to send
 * @param length Length of data to send (in/out: on return, contains the
 *        amount of data sent)
 * @param apiflags directly passed to tcp_write
 * @return the return value of tcp_write
 */
static err_t ftpd_data_write(struct tcp_pcb *pcb, const void* ptr, u16_t *length, u8_t apiflags)
{
	u16_t len;
	err_t err;
	LWIP_ASSERT("length != NULL", length != NULL);
	len = *length;
	do {
		//LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd-data: Sending %d bytes\n", len));
		err = tcp_write(pcb, ptr, len, apiflags);
		if (err == ERR_MEM) {
			if ((tcp_sndbuf(pcb) == 0) ||
					(pcb->snd_queuelen >= TCP_SND_QUEUELEN)) {
				/* no need to try smaller sizes */
				len = 1;
			} else {
				len /= 2;
			}
		}
	} while ((err == ERR_MEM) && (len > 1));

	*length = len;
	return err;
}
/**
 * The connection shall be actively closed.
 * Reset the sent- and recv-callbacks.
 *
 * @param pcb the tcp pcb to reset callbacks
 * @param hs connection state to free
 */
static void ftpd_close_data_conn(struct tcp_pcb *pcb, struct ftpd_datastate *hs)
{
	err_t err;
	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: Closing connection %p\n", (void*)pcb));
	tcp_arg(pcb, NULL);
	tcp_recv(pcb, NULL);
	tcp_err(pcb, NULL);
	tcp_poll(pcb, NULL, 0);
	tcp_sent(pcb, NULL);

	err = tcp_close(pcb);
	if(hs->msgfs->listenpcb) {
		tcp_close(hs->msgfs->listenpcb);
		hs->msgfs->listenpcb = NULL;
	}

	ftpd_data_state_free(hs);

	if (err != ERR_OK) {
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: Error %d closing %p\n", err, (void*)pcb));
		/* error closing, try again later in poll */
		tcp_poll(pcb, ftpd_data_poll, FTPD_POLL_INTERVAL);
	}
}
/**
 * The connection shall be actively closed.
 * Reset the sent- and recv-callbacks.
 *
 * @param pcb the tcp pcb to reset callbacks
 * @param hs connection state to free
 */
static void ftpd_close_conn(struct tcp_pcb *pcb, struct ftpd_msgstate *hs)
{
	err_t err;
	LWIP_DEBUGF1(FTDP_DEBUG, ("ftpd: Closing connection %p\n", (void*)pcb));
	/* data connection not closed */
	if(hs != NULL && hs->datapcb != NULL) {
		ftpd_close_data_conn(hs->datapcb, hs->datafs);
		hs->retries = FTPD_MAX_RETRIES;
		/* TODO Error Msg */
		LWIP_DEBUGF1(FTDP_DEBUG, ("ftpd: Error %d closing %p\n", err, (void*)pcb));
		/* error closing, try again later in poll */
		tcp_poll(pcb, ftpd_poll, FTPD_POLL_INTERVAL);
		return;
	}
	tcp_arg(pcb, NULL);
	tcp_recv(pcb, NULL);
	tcp_err(pcb, NULL);
	tcp_poll(pcb, NULL, 0);
	tcp_sent(pcb, NULL);
	ftpd_state_free(hs);

	err = tcp_close(pcb);
	if (err != ERR_OK) {
		LWIP_DEBUGF1(FTDP_DEBUG, ("ftpd: Error %d closing %p\n", err, (void*)pcb));
		/* error closing, try again later in poll */
		tcp_poll(pcb, ftpd_poll, FTPD_POLL_INTERVAL);
	}
}
/**
 * The poll function is called every 2nd second.
 * If there has been no data sent (which resets the retries) in 8 seconds, close.
 * If the last portion of a file has not been sent in 2 seconds, close.
 *
 * This could be increased, but we don't want to waste resources for bad connections.
 */
static err_t ftpd_data_poll(void *arg, struct tcp_pcb *pcb)
{
	struct ftpd_datastate *hs = (struct ftpd_datastate *)arg;
	LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_data_poll: pcb=%p hs=%p pcb_state=%s\n",
			(void*)pcb, (void*)hs, tcp_debug_state_str(pcb->state)));

	if (hs == NULL) {
		/* arg is null, close. */
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_data_poll: arg is NULL, close\n"));
		ftpd_close_data_conn(pcb, hs);
		return ERR_OK;
	} else {
		//hs->retries++;
		if (hs->retries == FTPD_MAX_RETRIES) {
			LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_data_poll: too many retries, close\n"));
			ftpd_close_data_conn(pcb, hs);
			return ERR_OK;
		}

		/* If this connection has a file open, try to send some more data. If
		 * it has not yet received a GET request, don't do this since it will
		 * cause the connection to close immediately. */
		//    if(hs && (hs->handle)) {
		//      LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_poll: try to send more data\n"));
		//      if(ftpd_send(pcb, hs)) {
		//        /* If we wrote anything to be sent, go ahead and send it now. */
		//        LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("tcp_output\n"));
		//        tcp_output(pcb);
		//      }
		//    }
	}

	return ERR_OK;
}
/**
 * The poll function is called every 2nd second.
 * If there has been no data sent (which resets the retries) in 8 seconds, close.
 * If the last portion of a file has not been sent in 2 seconds, close.
 *
 * This could be increased, but we don't want to waste resources for bad connections.
 */
static err_t ftpd_poll(void *arg, struct tcp_pcb *pcb)
{
	struct ftpd_msgstate *hs = (struct ftpd_msgstate *)arg;
	LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_poll: pcb=%p hs=%p pcb_state=%s\n",
			(void*)pcb, (void*)hs, tcp_debug_state_str(pcb->state)));

	if (hs == NULL) {
		/* arg is null, close. */
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_poll: arg is NULL, close\n"));
		ftpd_close_conn(pcb, hs);
		return ERR_OK;
	} else {
		/* TODO Autodisconnet nach xx Sekunden */
		if(hs->datafs == NULL || !hs->datafs->connected) {
			hs->timeout -= (FTPD_POLL_INTERVAL * TCP_SLOW_INTERVAL);
		}
		if(hs->timeout <= 0) {
			send_msg(pcb, hs, msg421_timeout, FTPD_TIMEOUT);
			ftpd_close_conn(pcb, hs);
		}
		/*hs->retries++;*/
		if (hs->retries == FTPD_MAX_RETRIES) {
			LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_poll: too many retries, close\n"));
			ftpd_close_conn(pcb, hs);
			return ERR_OK;
		}

		/* If this connection has a file open, try to send some more data. If
		 * it has not yet received a GET request, don't do this since it will
		 * cause the connection to close immediately. */
		//    if(hs && (hs->handle)) {
		//      LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_poll: try to send more data\n"));
		//      if(ftpd_send(pcb, hs)) {
		//        /* If we wrote anything to be sent, go ahead and send it now. */
		//        LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("tcp_output\n"));
		//        tcp_output(pcb);
		//      }
		//    }
	}

	return ERR_OK;
}
/**
 * Data has been received on this pcb.
 */
static err_t ftpd_data_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
{
	struct ftpd_datastate *hs = (struct ftpd_datastate *)arg;
	int blocksize;
	UINT len = 0;

	if(hs->buf_size < 512) {
		blocksize = hs->buf_size;
	} else if (hs->buf_size<1024) {
		blocksize = 512;
	} else if (hs->buf_size<2048) {
		blocksize = 1024;
	} else if (hs->buf_size<4096) {
		blocksize = 2048;
	} else  {
		blocksize = 4096;
	}
	LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_data_recv: pcb=%p pbuf=%p err=%s\n", (void*)pcb,
			(void*)p, lwip_strerr(err)));

	if (err == ERR_OK && p != NULL)
	{
		u16_t tot_len = 0;
		do {
			len = pbuf_copy_partial(p, (char*)(hs->buf + hs->unsend_bytes), (blocksize - hs->unsend_bytes), tot_len);
			tot_len += len;
			hs->unsend_bytes += len;
			if(hs->unsend_bytes == blocksize) {
				f_write(hs->fil, hs->buf, hs->unsend_bytes, &len);
				hs->unsend_bytes = 0;
				//LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd-data: %d written\n", len));
			}
		} while (p->tot_len > tot_len);
		tcp_recved(pcb, p->tot_len);
		pbuf_free(p);
	}
	if (err == ERR_OK && p == NULL)
	{
		struct ftpd_msgstate *fsm;
		struct tcp_pcb *msgpcb;
		if(hs->unsend_bytes) {
			f_write(hs->fil, hs->buf, hs->unsend_bytes, &len);
			hs->unsend_bytes -= len;
		}
		fsm = hs->msgfs;
		msgpcb = hs->msgpcb;
		f_close(hs->fil);
		mem_free(hs->fil);
		hs->fil = NULL;
		ftpd_close_data_conn(pcb, hs);
		fsm->datapcb = NULL;
		ftpd_data_state_free(fsm->datafs);
		fsm->datafs = NULL;
		fsm->state = FTPD_IDLE;
		send_msg(msgpcb, fsm, msg226);
	}
	return ERR_OK;
}
static void ftpd_send_file(struct ftpd_datastate *fsd, struct tcp_pcb *pcb)
{
	u16_t count;
	UINT read;
	if (!fsd->connected)
	{
		return;
	}
	if(tcp_sndqueuelen(pcb)>0) {
		tcp_output(pcb);
	}
	if(fsd->buf) {
		if(!fsd->filepos)
			count = tcp_mss(pcb);
		else
			count = fsd->buf_size;
	} else {
		return;
	}
	if(count > tcp_sndbuf(pcb)) count = tcp_sndbuf(pcb);
	f_read(fsd->fil, fsd->buf, count, &read);
	fsd->filepos += read;
	if(read > 0) {
		ftpd_data_write(pcb, fsd->buf, (u16_t *)&read, 0);
		tcp_output(pcb);
		if(f_size(fsd->fil) > fsd->filepos)
			return;
	}
	struct ftpd_msgstate *fsm;
	struct tcp_pcb *msgpcb;
	fsm = fsd->msgfs;
	msgpcb = fsd->msgpcb;
	f_close(fsd->fil);
	mem_free(fsd->fil);
	fsd->fil = NULL;
	ftpd_close_data_conn(pcb, fsd);
	fsm->datapcb = NULL;
	ftpd_data_state_free(fsm->datafs);
	fsm->state = FTPD_IDLE;
	send_msg(msgpcb, fsm, msg226);
}
/**
 * Data has been received on this pcb.
 */
static err_t ftpd_recv(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
{
	struct ftpd_msgstate *hs = (struct ftpd_msgstate *)arg;
	char *text = NULL;

	LWIP_DEBUGF1(ftpdD_DEBUG | LWIP_DBG_TRACE, ("ftpd_recv: pcb=%p pbuf=%p err=%s\n", (void*)pcb,
			(void*)p, lwip_strerr(err)));

	if ((err != ERR_OK) || (p == NULL) || (hs == NULL)) {
		/* error or closed by other side? */
		if (p != NULL) {
			/* Inform TCP that we have taken the data. */
			tcp_recved(pcb, p->tot_len);
			pbuf_free(p);
		}
		if (hs == NULL) {
			/* this should not happen, only to be robust */
			LWIP_DEBUGF1(FTPD_DEBUG, ("Error, ftpd_recv: hs is NULL, close\n"));
		}
		ftpd_close_conn(pcb, hs);
		return ERR_OK;
	}
	if (err == ERR_OK && p != NULL)
	{
		text = mem_malloc(p->tot_len + 1);

		if (text)
		{
			char cmd[5];
			struct pbuf *q;
			char *pt = text;
			struct ftpd_command *ftpd_cmd;

			for (q = p; q != NULL; q = q->next)
			{
				memcpy(pt, q->payload, q->len);
				pt += q->len;
			}
			*pt = '\0';

			pt = &text[strlen(text) - 1];
			while (((*pt == '\r') || (*pt == '\n')) && pt >= text)
			{
				*pt-- = '\0';
			}

			LWIP_DEBUGF1(FTPD_DEBUG, ("FTPD: query: %s \r\n", text));
			if (!strncmp(text, "CWD", 3) || !strncmp(text, "MKD", 3) || !strncmp(text, "RMD", 3))
			{
				strncpy(cmd, text, 3);
				for (pt = cmd; (pt  < &cmd[3]); pt++)
					*pt = toupper(*pt);
			} else
			{
				strncpy(cmd, text, 4);
				for (pt = cmd; (pt  < &cmd[4]); pt++)
					*pt = toupper(*pt);
			}
			*pt = '\0';

			for (ftpd_cmd = ftpd_commands; ftpd_cmd->cmd != NULL; ftpd_cmd++)
			{
				if (!strcmp(ftpd_cmd->cmd, cmd))
				{
					break;
				}
			}

			if (strlen(text) < (strlen(cmd) + 1))
			{
				pt = "";
			}
			else
			{
				pt = &text[strlen(cmd) + 1];
			}

			if (ftpd_cmd->func)
			{
				ftpd_cmd->func(pt, pcb, hs);
			}
			else
			{
				send_msg(pcb, hs, msg502);
			}
			mem_free(text);
			text = NULL;
			hs->timeout = FTPD_TIMEOUT * 1000;
		}
	}
	/* Inform TCP that we have taken the data. */
	tcp_recved(pcb, p->tot_len);
	pbuf_free(p);
	return ERR_OK;

	//  if (hs->handle == NULL) {
	//      parsed = ftpd_parse_request(&p, hs, pcb);
	//      LWIP_ASSERT("ftpd_parse_request: unexpected return value", parsed == ERR_OK
	//        || parsed == ERR_INPROGRESS ||parsed == ERR_ARG || parsed == ERR_USE);
	//  } else {
	//      LWIP_DEBUGF1(ftpdD_DEBUG, ("ftpd_recv: already sending data\n"));
	//  }
	//  if (p != NULL) {
	//      /* pbuf not passed to application, free it now */
	//    	pbuf_free(p);
	//  }
	//  if (parsed == ERR_OK) {
	//        LWIP_DEBUGF1(ftpdD_DEBUG | LWIP_DBG_TRACE, ("ftpd_recv: data %p len %"S32_F"\n", hs->file, hs->left));
	//        ftpd_send_data(pcb, hs);
	//  } else if (parsed == ERR_ARG) {
	//      /* @todo: close on ERR_USE? */
	//      ftpd_close_conn(pcb, hs);
	//  }
	//  return ERR_OK;
}
/** Allocate a struct ftpd_msgstate. */
static struct ftpd_msgstate* ftpd_msgstate_alloc(void)
		{
	struct ftpd_msgstate *ret;
#if ftpdD_USE_MEM_POOL
	ret = (struct ftpd_msgstate *)memp_malloc(MEMP_ftpd_msgstate);
#else /* ftpdD_USE_MEM_POOL */
	ret = (struct ftpd_msgstate *)mem_malloc(sizeof(struct ftpd_msgstate));
#endif /* ftpdD_USE_MEM_POOL */
	if (ret != NULL) {

		/* Initialize the structure. */
		memset(ret, 0, sizeof(struct ftpd_msgstate));
		ret->buf = (char*)mem_malloc((mem_size_t)300);
		ret->path[0] = '/';
		ret->path[1] = '\0';
	}
	return ret;
		}
/** Allocate a struct ftpd_msgstate. */
static struct ftpd_datastate* ftpd_datastate_alloc(void)
		{
	struct ftpd_datastate *ret;
#if ftpdD_USE_MEM_POOL
	ret = (struct ftpd_datastate *)memp_malloc(MEMP_ftpd_msgstate);
#else /* ftpdD_USE_MEM_POOL */
	ret = (struct ftpd_datastate *)mem_malloc(sizeof(struct ftpd_datastate));
#endif /* ftpdD_USE_MEM_POOL */
	if (ret != NULL) {
		/* Initialize the structure. */
		memset(ret, 0, sizeof(struct ftpd_datastate));
	}
	return ret;
		}
/**
 * The pcb had an error and is already deallocated.
 * The argument might still be valid (if != NULL).
 */
static void ftpd_data_err(void *arg, err_t err)
{
	struct ftpd_datastate *hs = (struct ftpd_datastate *)arg;
	LWIP_UNUSED_ARG(err);

	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_data_err: %s", lwip_strerr(err)));

	if (hs->msgfs->listenpcb)
	{
		tcp_close(hs->msgfs->listenpcb);
	}
	hs->msgfs->state = FTPD_IDLE;
	if (hs != NULL) {
		ftpd_data_state_free(hs);
	}
}
/**
 * The pcb had an error and is already deallocated.
 * The argument might still be valid (if != NULL).
 */
static void ftpd_err(void *arg, err_t err)
{
	struct ftpd_msgstate *hs = (struct ftpd_msgstate *)arg;
	LWIP_UNUSED_ARG(err);

	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_err: %s", lwip_strerr(err)));

	if (hs != NULL) {
		tcp_close(hs->listenpcb);
		tcp_close(hs->datapcb);
		ftpd_state_free(hs);
	}
}
/**
 * Data has been sent and acknowledged by the remote host.
 * This means that more data can be sent.
 */
static err_t ftpd_sent(void *arg, struct tcp_pcb *pcb, u16_t len)
{
	struct ftpd_msgstate *hs = (struct ftpd_msgstate *)arg;

	LWIP_DEBUGF1(FTDP_DEBUG | LWIP_DBG_TRACE, ("ftpd_sent %p\n", (void*)pcb));

	LWIP_UNUSED_ARG(len);

	if (hs == NULL) {
		return ERR_OK;
	}
	hs->retries = 0;
	if(hs->state == FTPD_QUIT)
	{
		send_msg(pcb, hs ,msg221);
		ftpd_close_conn(pcb, hs);
	}
	//ftp_send_data(pcb, hs);
	return ERR_OK;
}
static void send_next_directory(struct ftpd_datastate *fsd, struct tcp_pcb *pcb, int shortlist)
{

	FRESULT res;
	char buffer[300];
	u16_t len;

	if(fsd->fno == NULL) {
		fsd->fno = mem_malloc(sizeof(FILINFO));
		fsd->dir = mem_malloc(sizeof(DIR));
		if (fsd->fno == NULL || fsd->dir == NULL)
		{
			mem_free(fsd->fno);
			fsd->fno = NULL;
			LWIP_DEBUGF1(FTPD_DEBUG,  ("mem_malloc error! [%d, %s]\r\n", __LINE__, __FUNCTION__));
			return;
		}
		fsd->fno->lfname = Lfname;
		fsd->fno->lfsize = sizeof(Lfname);
		if(f_opendir(fsd->dir, fsd->msgfs->path) != 0) {
			/* TODO error bei Fehler */
		}
	}
	char month[4];
	/* Send until send space is low */
	while(tcp_sndbuf(pcb) > 300) {
		res = f_readdir(fsd->dir, fsd->fno);
		if(res != FR_OK || !fsd->fno->fname[0]) {
			struct ftpd_msgstate *fsm;
			struct tcp_pcb *msgpcb;
			fsm = fsd->msgfs;
			msgpcb = fsd->msgpcb;
			mem_free(fsd->fno);
			mem_free(fsd->dir);
			fsd->dir = NULL;
			fsd->fno = NULL;
			ftpd_close_data_conn(pcb, fsd);
			fsm->datapcb = NULL;
			fsm->datafs = NULL;
			fsm->state = FTPD_IDLE;
			send_msg(msgpcb, fsm, msg226);
			return;
		} else {
			if (shortlist)
			{
				len = sprintf(buffer, "%8lu %s\r\n", fsd->fno->fsize, (*Lfname ? Lfname : fsd->fno->fname));
				ftpd_data_write(pcb, &buffer, &len, TCP_WRITE_FLAG_COPY);
			}
			else
			{
				/*RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);*/
				strcpy((char*)month, ((fsd->fno->fdate >> 5) & 15 ? month_table[(((fsd->fno->fdate >> 5) & 15) - 1)] : month_table[0]));
				len = sprintf(buffer, "-rw-rw-rw- 1 enlogic ftp %11ld %s %02u %u %s\r\n", fsd->fno->fsize, month,  (fsd->fno->fdate & 31 ? fsd->fno->fdate & 31 : 1) , (fsd->fno->fdate >> 9) + 1980, (*Lfname ? Lfname : fsd->fno->fname));
				if (fsd->fno->fattrib & AM_DIR)
				{
					buffer[0] = 'd';
				}
				ftpd_data_write(pcb, &buffer, &len, TCP_WRITE_FLAG_COPY);
			}
		}
	}
	tcp_output(pcb);
}
/**
 * Data has been sent and acknowledged by the remote host.
 * This means that more data can be sent.
 */
static err_t ftpd_data_sent(void *arg, struct tcp_pcb *pcb, u16_t len)
{
	struct ftpd_datastate *hs = (struct ftpd_datastate *)arg;

	LWIP_DEBUGF1(FTPD_DEBUG | LWIP_DBG_TRACE, ("ftpd_data_sent %p\n", (void*)pcb));

	LWIP_UNUSED_ARG(len);

	if (hs == NULL) {
		return ERR_OK;
	}

	hs->retries = 0;

	switch (hs->msgfs->state)
	{
	case FTPD_LIST:
		send_next_directory(hs, pcb, 0);
		break;
	case FTPD_NLST:
		send_next_directory(hs, pcb, 1);
		break;
	case FTPD_RETR:
		ftpd_send_file(hs, pcb);
		break;
	default:
		break;
	}
	return ERR_OK;
}

static err_t ftpd_data_accept(void *arg, struct tcp_pcb *pcb, err_t err)
{
	struct ftpd_datastate *hs =  (struct ftpd_datastate *)arg;
	//struct tcp_pcb_listen *lpcb = (struct tcp_pcb_listen*)arg;
	LWIP_UNUSED_ARG(err);
	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_data_accept %p / %p\n", (void*)pcb, arg));

	/* Decrease the listen backlog counter */
	//  tcp_accepted(lpcb);
	/* Set priority */
	//tcp_setprio(pcb, FTPD_TCP_PRIO);
	/* Set up the various callback functions */
	tcp_recv(pcb, ftpd_data_recv);
	tcp_err(pcb, ftpd_data_err);
	tcp_poll(pcb, ftpd_data_poll, FTPD_POLL_INTERVAL);
	tcp_sent(pcb, ftpd_data_sent);

	hs->msgfs->datapcb = pcb;
	hs->connected = 1;

	switch (hs->msgfs->state)
	{
	case FTPD_LIST:
		send_next_directory(hs, pcb, 0);
		break;
	case FTPD_NLST:
		send_next_directory(hs, pcb, 1);
		break;
	case FTPD_RETR:
		ftpd_send_file(hs, pcb);
		break;
	default:
		break;
	}
	return ERR_OK;
}

static err_t ftpd_accept(void *arg, struct tcp_pcb *pcb, err_t err)
{
	struct ftpd_msgstate *hs;
	struct tcp_pcb_listen *lpcb = (struct tcp_pcb_listen*)arg;
	LWIP_UNUSED_ARG(err);
	LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_accept %p / %p\n", (void*)pcb, arg));

	/* Decrease the listen backlog counter */
	tcp_accepted(lpcb);
	/* Set priority */
	tcp_setprio(pcb, FTPD_TCP_PRIO);

	/* Allocate memory for the structure that holds the state of the
     connection - initialized by that function. */
	hs = ftpd_msgstate_alloc();
	if (hs == NULL) {
		LWIP_DEBUGF1(FTPD_DEBUG, ("ftpd_accept: Out of memory, RST\n"));
		return ERR_MEM;
	}
	ftpd_user_online++;
	hs->state = FTPD_USER;

	/* Tell TCP that this is the structure we wish to be passed for our
     callbacks. */
	tcp_arg(pcb, hs);

	/* Set up the various callback functions */
	tcp_recv(pcb, ftpd_recv);
	tcp_err(pcb, ftpd_err);
	tcp_poll(pcb, ftpd_poll, FTPD_POLL_INTERVAL);
	tcp_sent(pcb, ftpd_sent);
	if(ftpd_user_online > FTPD_MAX_CONNECTIONS) {
		send_msg(pcb, hs, msg421_user);
		ftpd_close_conn(pcb, hs);
	} else {
		send_msg(pcb, hs, msg220);
	}

	return ERR_OK;
}
void ftpd_init(void)
{
	struct tcp_pcb *pcb;
	err_t err;

	pcb = tcp_new();
	LWIP_ASSERT("ftpd_init: tcp_new failed", pcb != NULL);
	tcp_setprio(pcb, FTPD_TCP_PRIO);
	/* set SOF_REUSEADDR here to explicitly bind ftpd to multiple interfaces */
	err = tcp_bind(pcb, IP_ADDR_ANY, FTPD_SERVER_PORT);
	LWIP_ASSERT("ftpd_init: tcp_bind failed", err == ERR_OK);
	pcb = tcp_listen(pcb);
	LWIP_ASSERT("ftpd_init: tcp_listen failed", pcb != NULL);
	/* initialize callback arg and accept callback */
	tcp_arg(pcb, pcb);
	tcp_accept(pcb, ftpd_accept);
}
#endif /* LWIP_TCP */
