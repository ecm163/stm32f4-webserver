/**** RFM 12 library for Atmel AVR Microcontrollers *******
 *
 * This software is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 *
 * @author Peter Fuhrmann, Hans-Gert Dahmen, Soeren Heisrath
 */

/******************************************************
 *    THIS FILE IS BEING INCLUDED DIRECTLY		*
 *		(for performance reasons)				*
 ******************************************************/
 
//hardware spi helper macros
#define SS_ASSERT() GPIO_ResetBits(RFM12_CS_GPIO_PORT, RFM12_CS_PIN)
#define SS_RELEASE() GPIO_SetBits(RFM12_CS_GPIO_PORT, RFM12_CS_PIN)


#if RFM12_SPI_SOFTWARE
/* @description Actual sending function to send raw data to the Module
 * @note do NOT call this function directly, unless you know what you're doing.
 */
static uint8_t spi_data(uint8_t c)
{
	uint8_t x, d=d, i;
	for(x=0;x<8;x++){
		if(c & 0x80){
			GPIO_SetBits(RFM12_SPI_MOSI_GPIO_PORT, RFM12_SPI_MOSI_PIN);
			//PORT_MOSI |= (1<<BIT_MOSI);
		}else{
			GPIO_ResetBits(RFM12_SPI_MOSI_GPIO_PORT, RFM12_SPI_MOSI_PIN);
			//PORT_MOSI &= ~(1<<BIT_MOSI);
		}
		GPIO_SetBits(RFM12_SPI_SCK_GPIO_PORT, RFM12_SPI_SCK_PIN);
		for(i = 0; i<10;i++);
		//PORT_SCK |= (1<<BIT_SCK);
		d<<=1;
		if(GPIO_ReadInputDataBit(RFM12_SPI_MISO_GPIO_PORT,RFM12_SPI_MISO_PIN) == Bit_SET){
			d|=1;
		}
		GPIO_ResetBits(RFM12_SPI_SCK_GPIO_PORT, RFM12_SPI_SCK_PIN);
		//PORT_SCK &= ~(1<<BIT_SCK);
		c<<=1;
	}
	return d;
}
#endif


//non-inlined version of rfm12_data
//warning: without the attribute, gcc will inline this even if -Os is set
void __attribute__ ((noinline)) rfm12_data(uint16_t d)
{
	SS_ASSERT();
	#if !(RFM12_SPI_SOFTWARE)


	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, d>>8);
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);

	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI,(d & 0xff));
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);

	#else
	spi_data(d >> 8   );
	spi_data(d &  0xff);
	#endif
	SS_RELEASE();
}


//non-inlined version of rfm12_read
//warning: without the attribute, gcc will inline this even if -Os is set
uint16_t __attribute__ ((noinline)) rfm12_read(uint16_t c)
{
	uint16_t retval;
	SS_ASSERT();
	
	#if !(RFM12_SPI_SOFTWARE)
	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, (c>>8));
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);
	/*!< Wait to receive a byte */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	/*!< Return the byte read from the SPI bus */
	retval = SPI_I2S_ReceiveData(RFM12_SPI);
	retval <<= 8;
	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, (c & 0xff));
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);
	/*!< Wait to receive a byte */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	/*!< Return the byte read from the SPI bus */
	retval |= (SPI_I2S_ReceiveData(RFM12_SPI) & 0xff);
	
	#else
	retval =  spi_data(c >> 8   );
	retval <<= 8;
	retval |= spi_data(c &  0xff);
	#endif
	SS_RELEASE();
	return retval;
}


/* @description reads the upper 8 bits of the status
 * register (the interrupt flags)
 */
 uint8_t rfm12_read_int_flags_inline()
{
	SS_ASSERT();
	#if !(RFM12_SPI_SOFTWARE)
	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, 0);
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	SS_RELEASE();
	return SPI_I2S_ReceiveData(RFM12_SPI);

	#else
	unsigned char x, d=d, i;
	GPIO_ResetBits(RFM12_SPI_MOSI_GPIO_PORT, RFM12_SPI_MOSI_PIN);
	//PORT_MOSI &= ~(1<<BIT_MOSI);
	for(x=0;x<8;x++){
		GPIO_SetBits(RFM12_SPI_SCK_GPIO_PORT, RFM12_SPI_SCK_PIN);
		for(i = 0; i<10;i++);
		//PORT_SCK |= (1<<BIT_SCK);
		d<<=1;
		if(GPIO_ReadInputDataBit(RFM12_SPI_MISO_GPIO_PORT,RFM12_SPI_MISO_PIN) == Bit_SET){
			d|=1;
		}
		GPIO_ResetBits(RFM12_SPI_SCK_GPIO_PORT, RFM12_SPI_SCK_PIN);
		//PORT_SCK &= ~(1<<BIT_SCK);
	}
	SS_RELEASE();
	return d;
	#endif
}


/* @description inline version of rfm12_data for use in interrupt
 */
void rfm12_data_inline(uint8_t cmd, uint8_t d)
{
	SS_ASSERT();
	#if !(RFM12_SPI_SOFTWARE)

	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, cmd);
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);

	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, d);
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);

	#else
	spi_data( cmd );
	spi_data( d   );
	#endif
	SS_RELEASE();
}


/* @description inline function for reading the fifo
 */
uint8_t rfm12_read_fifo_inline()
{
	SS_ASSERT();


	#if !(RFM12_SPI_SOFTWARE)
	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, ( RFM12_CMD_READ >> 8 ));
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);
	/*!< Send byte through the SPI1 peripheral */
	SPI_I2S_SendData(RFM12_SPI, 0);
	/*!< Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_TXE) == RESET);
	/*!< Wait to receive a byte */
	while (SPI_I2S_GetFlagStatus(RFM12_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	/*!< Return the byte read from the SPI bus */
	SS_RELEASE();
	return SPI_I2S_ReceiveData(RFM12_SPI);
	
	#else
	uint8_t retval;
	spi_data( RFM12_CMD_READ >> 8 );
	retval = spi_data( 0   );

	SS_RELEASE();
	return retval;
	#endif
}

/**
  * @brief  DeInitializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void RFM12_LowLevel_DeInit(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /*!< Disable the RFM12_SPI  ************************************************/
  SPI_Cmd(RFM12_SPI, DISABLE);

  /*!< DeInitializes the RFM12_SPI *******************************************/
  SPI_I2S_DeInit(RFM12_SPI);

  /*!< RFM12_SPI Periph clock disable ****************************************/
  RFM12_SPI_CLK_INIT(RFM12_SPI_CLK, DISABLE);

  /*!< Configure all pins used by the SPI as input floating *******************/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  GPIO_InitStructure.GPIO_Pin = RFM12_SPI_SCK_PIN;
  GPIO_Init(RFM12_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RFM12_SPI_MISO_PIN;
  GPIO_Init(RFM12_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RFM12_SPI_MOSI_PIN;
  GPIO_Init(RFM12_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RFM12_CS_PIN;
  GPIO_Init(RFM12_CS_GPIO_PORT, &GPIO_InitStructure);
}
/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void RFM12_LowLevel_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /*!< Enable GPIO clocks */
  RCC_AHB1PeriphClockCmd(RFM12_SPI_SCK_GPIO_CLK | RFM12_SPI_MISO_GPIO_CLK |
                         RFM12_SPI_MOSI_GPIO_CLK | RFM12_CS_GPIO_CLK, ENABLE);

  /*!< SPI pins configuration *************************************************/

#if RFM12_SPI_SOFTWARE == 0
  /*!< Enable the SPI clock */
  RFM12_SPI_CLK_INIT(RFM12_SPI_CLK, ENABLE);

  GPIO_PinAFConfig(RFM12_SPI_SCK_GPIO_PORT, RFM12_SPI_SCK_SOURCE, RFM12_SPI_SCK_AF);
  GPIO_PinAFConfig(RFM12_SPI_MISO_GPIO_PORT, RFM12_SPI_MISO_SOURCE, RFM12_SPI_MISO_AF);
  GPIO_PinAFConfig(RFM12_SPI_MOSI_GPIO_PORT, RFM12_SPI_MOSI_SOURCE, RFM12_SPI_MOSI_AF);
#endif

  /*!< Connect SPI pins to AF5 */
#if RFM12_SPI_SOFTWARE == 0
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
#else
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
#endif
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;

  /*!< SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = RFM12_SPI_SCK_PIN;
  GPIO_Init(RFM12_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  /*!< SPI MOSI pin configuration */
  GPIO_InitStructure.GPIO_Pin =  RFM12_SPI_MOSI_PIN;
  GPIO_Init(RFM12_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /*!< SPI MISO pin configuration */
#if RFM12_SPI_SOFTWARE
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
#endif
  GPIO_InitStructure.GPIO_Pin =  RFM12_SPI_MISO_PIN;
  GPIO_Init(RFM12_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  /*!< Configure RFM12 CS pin in output pushpull mode ********************/
  GPIO_InitStructure.GPIO_Pin = RFM12_CS_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(RFM12_CS_GPIO_PORT, &GPIO_InitStructure);
}
/**
  * @brief  DeInitializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void RFM12_DeInit(void)
{
  RFM12_LowLevel_DeInit();
}

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void spi_init(void)
{
  RFM12_LowLevel_Init();
  /*!< Deselect the FLASH: Chip Select high */
  SS_RELEASE();
#if RFM12_SPI_SOFTWARE == 0
  SPI_InitTypeDef  SPI_InitStructure;

  SPI_Cmd(RFM12_SPI, DISABLE);

  /*!< SPI configuration */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;

  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(RFM12_SPI, &SPI_InitStructure);

  /*!< Enable the RFM12_SPI  */
  SPI_Cmd(RFM12_SPI, ENABLE);
#endif
}

