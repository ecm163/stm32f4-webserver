/*
 * rtc.h
 *
 *  Created on: Jan 29, 2012
 *      Author: black
 */

#ifndef RTC_H_
#define RTC_H_

#include <stdio.h>
#include <time.h>
#include "main.h"

/* Private define ------------------------------------------------------------*/
/* Uncomment the corresponding line to select the RTC Clock source */
//#define RTC_CLOCK_SOURCE_LSE   /* LSE used as RTC source clock */
#define RTC_CLOCK_SOURCE_LSI  /* LSI used as RTC source clock. The RTC Clock
                                      may varies due to LSI frequency dispersion. */
void rtc_init(void);
void RTC_Config(void);
uint32_t GetLSIFrequency(void);
void RTC_TimeShow(void);
void RTC_DateShow(void);
void RTC_TimeStampShow(void);
uint32_t current_timestamp(void);
uint32_t get_timestamp(RTC_DateTypeDef date, RTC_TimeTypeDef time);

extern RTC_TimeTypeDef RTC_TimeStructure;
extern RTC_DateTypeDef RTC_DateStructure;

#endif /* RTC_H_ */
