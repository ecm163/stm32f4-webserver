/********************************************************************************/
/*!
	@file			uart_support.c
	@author         Nemui Trinomius (http://nemuisan.blog.bai.ne.jp)
    @version        2.00
    @date           2011.09.14
	@brief          Based on ST Microelectronics's Sample Thanks!

    @section HISTORY
		2011.06.12	V1.00	Start Here.
		2011.09.14	V2.00	Expand fifo buffer uint8_t to uint16_t

    @section LICENSE
		BSD License. See Copyright.txt
*/
/********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* Defines -------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/
USART_InitTypeDef USART_InitStructure;
static USART_Buffer_t* pUSART_Buf;
USART_Buffer_t USART_Buf;

/* Constants -----------------------------------------------------------------*/

/* Function prototypes -------------------------------------------------------*/

/* Functions -----------------------------------------------------------------*/
#if defined(__CROSSWORKS_ARM)    // Crossstudio
int putchar(int c) {
	putch((uint8_t) c);
	return 1;
}
int __getchar() {
	return (int)getch();
}
#else // Atollic
// Redirecting of printf to UARTx - this works for Atollic
#include <unistd.h>
int _write_r(void *reent, int fd, char *ptr, size_t len) {
	size_t counter = len;

	while(counter-- > 0) {
		putch((uint8_t)(*ptr));
		ptr++;
	}

	return len;
}
int _read_r (void *reent, uint16_t fd, char *ptr, uint32_t len)
{
        uint32_t counter = len;

        if(fd != STDIN_FILENO) {
                return len;
        }
        while(counter-- > 0) {                          // Read the character to the buffer from UART
                while (USART_RXBufferData_Available(pUSART_Buf) == false);
                *ptr = getch();
                ptr++;
        }
        return len;
}

#endif
/**************************************************************************/
/*!
    Initialize UART.
*/
/**************************************************************************/
/* Initialize serial console */
void usart_init(uint32_t baudrate)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable GPIO clock */
	RCC_AHB1PeriphClockCmd(RCC_GPIO_PERIPH, ENABLE);
	/* Enable UART clock */
	RCC_APB2PeriphClockCmd(RCC_USART_PERIPH, ENABLE);

	/* Connect PXx to USARTx_Tx*/
	GPIO_PinAFConfig(UART_GPIO,  UART_TX_PIN_SOURCE, UART_GPIO_AF);
	/* Connect PXx to USARTx_Rx*/
	GPIO_PinAFConfig(UART_GPIO,  UART_RX_PIN_SOURCE, UART_GPIO_AF);

	/* Configure USART Tx as alternate function  */
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

	GPIO_InitStructure.GPIO_Pin = UART_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(UART_GPIO, &GPIO_InitStructure);

	/* Configure USART Rx as alternate function  */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = UART_RX_PIN;
	GPIO_Init(UART_GPIO, &GPIO_InitStructure);

	/* Configure one bit for preemption priority */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	/* Enable the USART1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = UART_NIRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_Init(UART, &USART_InitStructure);

	/* Init Ring Buffer */
	pUSART_Buf = &USART_Buf;
	USART_Buf.RX_Tail = 0;
	USART_Buf.RX_Head = 0;
	USART_Buf.TX_Tail = 0;
	USART_Buf.TX_Head = 0;

	/* Enable USART Receive interrupts */
	USART_ITConfig(UART, USART_IT_RXNE, ENABLE);
	USART_Cmd(UART, ENABLE);

}

/**************************************************************************/
/*!
    Check UART TX Buffer Empty.
*/
/**************************************************************************/
bool USART_TXBuffer_FreeSpace(USART_Buffer_t* USART_buf)
{
	/* Make copies to make sure that volatile access is specified. */
	uint16_t tempHead = (USART_buf->TX_Head + 1) & (UART_TX_BUFSIZE-1);
	uint16_t tempTail = USART_buf->TX_Tail;

	/* There are data left in the buffer unless Head and Tail are equal. */
	return (tempHead != tempTail);
}

/**************************************************************************/
/*!
    Put Bytedata with Buffering.
*/
/**************************************************************************/
bool USART_TXBuffer_PutByte(USART_Buffer_t* USART_buf, uint8_t data)
{

	uint16_t tempTX_Head;
	bool TXBuffer_FreeSpace;

	TXBuffer_FreeSpace = USART_TXBuffer_FreeSpace(USART_buf);


	if(TXBuffer_FreeSpace)
	{
	  	tempTX_Head = USART_buf->TX_Head;

		__disable_irq();
	  	USART_buf->TX[tempTX_Head]= data;
		/* Advance buffer head. */
		USART_buf->TX_Head = (tempTX_Head + 1) & (UART_TX_BUFSIZE-1);
		__enable_irq();

		/* Enable TXE interrupt. */
		USART_ITConfig(UART, USART_IT_TXE, ENABLE);
	}
	return TXBuffer_FreeSpace;
}

/**************************************************************************/
/*!
    Check UART RX Buffer Empty.
*/
/**************************************************************************/
bool USART_RXBufferData_Available(USART_Buffer_t* USART_buf)
{
	/* Make copies to make sure that volatile access is specified. */
	uint16_t tempHead = USART_buf->RX_Head;
	uint16_t tempTail = USART_buf->RX_Tail;

	/* There are data left in the buffer unless Head and Tail are equal. */
	return (tempHead != tempTail);
}

/**************************************************************************/
/*!
    Get Bytedata with Buffering.
*/
/**************************************************************************/
uint8_t USART_RXBuffer_GetByte(USART_Buffer_t* USART_buf)
{
	uint8_t ans;

	__disable_irq();
	ans = (USART_buf->RX[USART_buf->RX_Tail]);

	/* Advance buffer tail. */
	USART_buf->RX_Tail = (USART_buf->RX_Tail + 1) & (UART_RX_BUFSIZE-1);

	__enable_irq();

	return ans;
}

/**************************************************************************/
/*!
    High Level function.
*/
/**************************************************************************/
/* Send 1 character */
inline void putch(uint8_t data)
{
#if 1
	/* Polling version */
	while (!(UART->SR & USART_FLAG_TXE));
	UART->DR = data;
#else
	/* Interrupt Version */
	while(!USART_TXBuffer_FreeSpace(pUSART_Buf));
	USART_TXBuffer_PutByte(pUSART_Buf,data);
#endif
}

/**************************************************************************/
/*!
    High Level function.
*/
/**************************************************************************/
/* Receive 1 character */
uint8_t getch(void)
{
	if (USART_RXBufferData_Available(pUSART_Buf))  return USART_RXBuffer_GetByte(pUSART_Buf);
	else										   return false;
}

/**************************************************************************/
/*!
    High Level function.
*/
/**************************************************************************/
/* Return 1 if key pressed */
uint8_t keypressed(void)
{
  return (USART_RXBufferData_Available(pUSART_Buf));
  /*return (UART->SR & USART_FLAG_RXNE);*/
}

/**************************************************************************/
/*!
    High Level function.
*/
/**************************************************************************/
/* Send a string */
void cputs(char *s)
{
  while (*s)
    putch(*s++);
}

/**************************************************************************/
/*!
    High Level function.
*/
/**************************************************************************/
/* Receive a string, with rudimentary line editing */
void cgets(char *s, int bufsize)
{
  char *p;
  int c;

  memset(s, 0, bufsize);

  p = s;

  for (p = s; p < s + bufsize-1;)
  {
    /* 20090521Nemui */
	do{
		c = getch();
	}while(c == false);
	/* 20090521Nemui */
    switch (c)
    {
      case '\r' :
      case '\n' :
        putch('\r');
        putch('\n');
        *p = '\n';
        return;

      case '\b' :
        if (p > s)
        {
          *p-- = 0;
          putch('\b');
          putch(' ');
          putch('\b');
        }
        break;

      default :
        putch(c);
        *p++ = c;
        break;
    }
  }

  return;
}
/**************************************************************************/
/*!
    @brief	Handles USARTx global interrupt request.
	@param	None.
    @retval	None.
*/
/**************************************************************************/
void UART_NIRQ_HANDLER (void)
{

	if(USART_GetITStatus(UART, USART_IT_RXNE) != RESET)
	{
		/* Advance buffer head. */
		uint16_t tempRX_Head = ((&USART_Buf)->RX_Head + 1) & (UART_RX_BUFSIZE-1);

		/* Check for overflow. */
		uint16_t tempRX_Tail = (&USART_Buf)->RX_Tail;
		uint8_t data =  USART_ReceiveData(UART);

		if (tempRX_Head == tempRX_Tail) {
			/* Disable the USART2 Receive interrupt */
			USART_ITConfig(UART, USART_IT_RXNE, DISABLE);
		}else{
			(&USART_Buf)->RX[(&USART_Buf)->RX_Head] = data;
			(&USART_Buf)->RX_Head = tempRX_Head;
		}

	}

	if(USART_GetITStatus(UART, USART_IT_TXE) != RESET)
	{
		/* Check if all data is transmitted. */
		uint16_t tempTX_Tail = (&USART_Buf)->TX_Tail;
		if ((&USART_Buf)->TX_Head == tempTX_Tail){
			/* Overflow MAX size Situation */
			/* Disable the USART2 Transmit interrupt */
			USART_ITConfig(UART, USART_IT_TXE, DISABLE);
		}else{
			/* Start transmitting. */
			uint8_t data = (&USART_Buf)->TX[(&USART_Buf)->TX_Tail];
			UART->DR = data;

			/* Advance buffer tail. */
			(&USART_Buf)->TX_Tail = ((&USART_Buf)->TX_Tail + 1) & (UART_TX_BUFSIZE-1);
		}

	}
}
/* End Of File ---------------------------------------------------------------*/
