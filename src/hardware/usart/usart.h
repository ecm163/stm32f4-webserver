/********************************************************************************/
/*!
	@file			uart_support.c
	@author         Nemui Trinomius (http://nemuisan.blog.bai.ne.jp)
    @version        1.00
    @date           2011.10.16
	@brief          Based on ST Microelectronics's Sample Thanks!

    @section HISTORY
		2011.10.16	V1.00	Start Here.

    @section LICENSE
		BSD License. See Copyright.txt
*/
/********************************************************************************/

#ifndef USART_H_
#define USART_H_

#ifdef __cplusplus
 extern "C" {
#endif

/* General Inclusion */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>

/* uC Specific Inclusion */
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"
#include "stm32f4xx_conf.h"

/* USART Definition */
#define UART_RX_BUFSIZE		64
#define UART_TX_BUFSIZE		512
#define UART_BAUDRATE		256000UL

#if defined (USE_STM32F4_DISCOVERY)
 	#define UART				USART1
	#define UART_GPIO			GPIOB
	#define UART_GPIO_AF		GPIO_AF_USART1
	#define UART_TX_PIN_SOURCE	GPIO_PinSource6
	#define UART_RX_PIN_SOURCE	GPIO_PinSource7
	#define UART_TX_PIN			GPIO_Pin_6
	#define UART_RX_PIN			GPIO_Pin_7
    #define RCC_GPIO_PERIPH		RCC_AHB1Periph_GPIOB
	#define RCC_USART_PERIPH 	RCC_APB2Periph_USART1
	#define UART_NIRQ			USART1_IRQn
    #define UART_NIRQ_HANDLER	USART1_IRQHandler
#else
 #error uart
#endif

/* General Definition */
#define countof(a)   (sizeof(a) / sizeof(*(a)))

/* Funcion Prototypes */
extern void usart_init(uint32_t baudrate);
extern void putch(uint8_t c);
extern uint8_t getch(void);
extern uint8_t keypressed(void);
extern void cputs(char *s);
extern void cgets(char *s, int bufsize);

/* Structs */
/* This is From AVRX uC Sample!!! */
/* @brief USART transmit and receive ring buffer. */
typedef struct USART_Buffer
{
	/* @brief Receive buffer. */
	volatile uint8_t RX[UART_RX_BUFSIZE];
	/* @brief Transmit buffer. */
	volatile uint8_t TX[UART_TX_BUFSIZE];
	/* @brief Receive buffer head. */
	volatile uint16_t RX_Head;
	/* @brief Receive buffer tail. */
	volatile uint16_t RX_Tail;
	/* @brief Transmit buffer head. */
	volatile uint16_t TX_Head;
	/* @brief Transmit buffer tail. */
	volatile uint16_t TX_Tail;
} USART_Buffer_t;

/* Externs */
extern USART_InitTypeDef USART_InitStructure;
extern USART_Buffer_t USART_Buf;

bool USART_TXBuffer_FreeSpace(USART_Buffer_t* USART_buf);
bool USART_TXBuffer_PutByte(USART_Buffer_t* USART_buf, uint8_t data);
bool USART_RXBufferData_Available(USART_Buffer_t* USART_buf);
uint8_t USART_RXBuffer_GetByte(USART_Buffer_t* USART_buf);

#ifdef __cplusplus
}
#endif


#endif /* USART_H_ */
