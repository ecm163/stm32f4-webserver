#include <stdint.h>

// SHT11 hum/temp sensor


#define SHT11_AT_5V         4010
#define SHT11_AT_4V         3980
#define SHT11_AT_3_5V       3970
#define SHT11_AT_3V         3960
#define SHT11_AT_2_5V       3940

#define SHT11_TEMP_V_COMP   SHT11_AT_3V

#define SHT11_RES_LOW       1  //8_12_Bit
#define SHT11_RES_HIGH      0  //12_14_Bit
#define SHT11_RESOLUTION    SHT11_RES_HIGH


/* SHT11 CLK PC.7 */
#define SHT11_SCK_PIN                   GPIO_Pin_7
#define SHT11_SCK_GPIO_PORT             GPIOC
#define SHT11_SCK_GPIO_CLK              RCC_AHB1Periph_GPIOC
#define SHT11_SCK_SOURCE                GPIO_PinSource7

/* STH11 SDO PC.6 */
#define SHT11_SDA_PIN                  GPIO_Pin_6
#define SHT11_SDA_GPIO_PORT            GPIOC
#define SHT11_SDA_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define SHT11_SDA_SOURCE               GPIO_PinSource6

