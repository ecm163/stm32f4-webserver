/*
    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.

    $Id$
*/

#include "mirf.h"

// Defines for setting the MiRF registers for transmitting or receiving mode
#define TX_POWERUP mirf_config_register(CONFIG, mirf_CONFIG | ( (1<<PWR_UP) | (0<<PRIM_RX) ) )
#define RX_POWERUP mirf_config_register(CONFIG, mirf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) )



// Flag which denotes transmitting mode
volatile uint8_t PTX;

void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len)
// Shift full array through target device
{
       uint8_t i;
       for (i = 0; i < len; i++) {
         	SPI_I2S_SendData(NRF_SPI, dataout[i]);
         	while (SPI_I2S_GetFlagStatus(NRF_SPI, SPI_I2S_FLAG_TXE) == RESET);
         	while (SPI_I2S_GetFlagStatus(NRF_SPI, SPI_I2S_FLAG_RXNE) == RESET);
         	datain[i] = (uint8_t)SPI_I2S_ReceiveData(NRF_SPI);
       }
}

void spi_transmit_sync (uint8_t * dataout, uint8_t len)
// Shift full array to target device without receiving any byte
{
       uint8_t i;
       for (i = 0; i < len; i++) {
        	SPI_I2S_SendData(NRF_SPI, dataout[i]);
        	while (SPI_I2S_GetFlagStatus(NRF_SPI, SPI_I2S_FLAG_TXE) == RESET);
       }
}

uint8_t spi_fast_shift (uint8_t data)
// Clocks only one byte to target device and returns the received one
{
	SPI_I2S_SendData(NRF_SPI, data);
	while (SPI_I2S_GetFlagStatus(NRF_SPI, SPI_I2S_FLAG_TXE) == RESET);
 	while (SPI_I2S_GetFlagStatus(NRF_SPI, SPI_I2S_FLAG_RXNE) == RESET);
 	return (uint8_t)SPI_I2S_ReceiveData(NRF_SPI);
}
/**
  * @brief  Configures EXTI Line0 (connected to PA0 pin) in interrupt mode
  * @param  None
  * @retval None
  */
void NIRQ_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOA clock */
  RCC_AHB1PeriphClockCmd(NRF_nIRQ_GPIO_CLK, ENABLE);
  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  /* Configure PA0 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = NRF_nIRQ_PIN;
  GPIO_Init(NRF_nIRQ_GPIO_PORT, &GPIO_InitStructure);

  /* Connect EXTI Line0 to PA0 pin */
  SYSCFG_EXTILineConfig(NRF_nIRQ_EXTI_PORT, NRF_nIRQ_EXTI_PIN);

  /* Configure EXTI Line0 */
  EXTI_InitStructure.EXTI_Line = NRF_nIRQ_EXTI_Line;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line0 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = NRF_nIRQ_EXTI_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
/**
  * @brief  DeInitializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void NRF_LowLevel_DeInit(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /*!< Disable the NRF_SPI  ************************************************/
  SPI_Cmd(NRF_SPI, DISABLE);

  /*!< DeInitializes the NRF_SPI *******************************************/
  SPI_I2S_DeInit(NRF_SPI);

  /*!< NRF_SPI Periph clock disable ****************************************/
  NRF_SPI_CLK_INIT(NRF_SPI_CLK, DISABLE);

  /*!< Configure all pins used by the SPI as input floating *******************/
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  GPIO_InitStructure.GPIO_Pin = NRF_SPI_SCK_PIN;
  GPIO_Init(NRF_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = NRF_SPI_MISO_PIN;
  GPIO_Init(NRF_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = NRF_SPI_MOSI_PIN;
  GPIO_Init(NRF_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = NRF_CSN_PIN;
  GPIO_Init(NRF_CSN_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = NRF_CE_PIN;
  GPIO_Init(NRF_CE_GPIO_PORT, &GPIO_InitStructure);
}
/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void NRF_LowLevel_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /*!< Enable the SPI clock */
  NRF_SPI_CLK_INIT(NRF_SPI_CLK, ENABLE);

  /*!< Enable GPIO clocks */
  RCC_AHB1PeriphClockCmd(NRF_SPI_SCK_GPIO_CLK | NRF_SPI_MISO_GPIO_CLK |
                         NRF_SPI_MOSI_GPIO_CLK | NRF_CSN_GPIO_CLK | NRF_CE_GPIO_CLK, ENABLE);

  /*!< SPI pins configuration *************************************************/

  /*!< Connect SPI pins to AF5 */
  GPIO_PinAFConfig(NRF_SPI_SCK_GPIO_PORT, NRF_SPI_SCK_SOURCE, NRF_SPI_SCK_AF);
  GPIO_PinAFConfig(NRF_SPI_MISO_GPIO_PORT, NRF_SPI_MISO_SOURCE, NRF_SPI_MISO_AF);
  GPIO_PinAFConfig(NRF_SPI_MOSI_GPIO_PORT, NRF_SPI_MOSI_SOURCE, NRF_SPI_MOSI_AF);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;

  /*!< SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = NRF_SPI_SCK_PIN;
  GPIO_Init(NRF_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

  /*!< SPI MOSI pin configuration */
  GPIO_InitStructure.GPIO_Pin =  NRF_SPI_MOSI_PIN;
  GPIO_Init(NRF_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /*!< SPI MISO pin configuration */
  GPIO_InitStructure.GPIO_Pin =  NRF_SPI_MISO_PIN;
  GPIO_Init(NRF_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

  /*!< Configure NRF CS pin in output pushpull mode ********************/
  GPIO_InitStructure.GPIO_Pin = NRF_CSN_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(NRF_CSN_GPIO_PORT, &GPIO_InitStructure);

  /*!< Configure NRF CS pin in output pushpull mode ********************/
  GPIO_InitStructure.GPIO_Pin = NRF_CE_PIN;
  GPIO_Init(NRF_CE_GPIO_PORT, &GPIO_InitStructure);

  // Define CSN and CE as Output and set them to default
  mirf_CE_lo;
  mirf_CSN_hi;
}
/**
  * @brief  DeInitializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void NRF_DeInit(void)
{
  NRF_LowLevel_DeInit();
}

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void nrf_spi_init(void)
{
  SPI_InitTypeDef  SPI_InitStructure;

  NRF_LowLevel_Init();

  /*!< SPI configuration */
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;

  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_Init(NRF_SPI, &SPI_InitStructure);

  /*!< Enable the NRF_SPI  */
  SPI_Cmd(NRF_SPI, ENABLE);
}


void mirf_init() 
// Initializes pins ans interrupt to communicate with the MiRF module
// Should be called in the early initializing phase at startup.
{


#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega32__)
    // Initialize external interrupt 0 (PD2)
    //MCUCR = ((1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00)); // Set external interupt on falling edge
    //GICR  = ((0<<INT1)|(1<<INT0));                         // Activate INT0
#endif // __AVR_ATmega8__

#if defined(__AVR_ATmega168__)
    // Initialize external interrupt on port PD6 (PCINT22)
    DDRB &= ~(1<<PD6);
    PCMSK2 = (1<<PCINT22);
    PCICR  = (1<<PCIE2);
#endif // __AVR_ATmega168__    

    // Initialize spi module
    nrf_spi_init();

    NIRQ_Config();
}


void mirf_config() 
// Sets the important registers in the MiRF module and powers the module
// in receiving mode
{
    // Set RF channel
	mirf_config_register(EN_AA,0x01);      	// Enable Auto.Ack:Pipe0
	mirf_config_register(EN_RXADDR,0x01);  // Enable Pipe0
	mirf_config_register(SETUP_RETR, 0x1a);	///500us + 86us, 10 retrans...
    mirf_config_register(RF_CH,mirf_CH);
	mirf_config_register(RF_SETUP, 0x26);   // TX_PWR:0dBm, Datarate:2Mbps, LNA:HCURR

	mirf_config_register(FEATURE, (1<<EN_DPL));   
	mirf_config_register(DYNPD, (1<<DPL_P0));   

    // Set length of incoming payload 
    //mirf_config_register(RX_PW_P0, mirf_PAYLOAD);

    // Start receiver 
    PTX = 0;        // Start in receiving mode
    RX_POWERUP;     // Power up in receiving mode
    mirf_CE_hi;     // Listening for pakets
}

void mirf_set_RADDR(uint8_t * adr) 
// Sets the receiving address
{
    mirf_CE_lo;
    mirf_write_register(RX_ADDR_P0,adr,5);
    mirf_CE_hi;
}

void mirf_set_TADDR(uint8_t * adr)
// Sets the transmitting address
{
    mirf_write_register(TX_ADDR, adr,5);
}
extern void polling() 
{
	uint8_t status;   
    mirf_CSN_lo;                                // Pull down chip select
    status = spi_fast_shift(NOP);               // Read status register
    mirf_CSN_hi;                                // Pull up chip select	

 	if(status & (1<<TX_DS)) 					//TXFinish
	{
        mirf_CE_lo;                             // Deactivate transreceiver
        RX_POWERUP;                             // Power up in receiving mode
        mirf_CE_hi;                             // Listening for pakets
        PTX = 0;                                // Set to receiving mode

        // Reset status register for further interaction
        mirf_config_register(STATUS,(1<<TX_DS)); // Reset status register
		printf("TX finish\n");

	}
	if (status & (1<<MAX_RT)) {					// Timeout
        mirf_CE_lo;   

	    mirf_CSN_lo;                    	// Pull down chip select
	    spi_fast_shift( FLUSH_TX );     	// Write cmd to flush tx fifo
	    mirf_CSN_hi;                    	// Pull up chip select
						                         // Deactivate transreceiver
        RX_POWERUP;                             // Power up in receiving mode
        mirf_CE_hi;                             // Listening for pakets
        PTX = 0;                                // Set to receiving mode
		mirf_config_register(STATUS,(1<<MAX_RT)); // Reset status register
		printf("Max TR\n");
	}
}
#if defined(__AVR_ATmega8__) || defined(__AVR_ATmega32__)
SIGNAL(SIG_INTERRUPT0) 
#endif // __AVR_ATmega8__
#if defined(__AVR_ATmega168__)
SIGNAL(SIG_PIN_CHANGE2) 
#endif // __AVR_ATmega168__  
// Interrupt handler 
NRF_ISR
{
	if(EXTI_GetITStatus(NRF_nIRQ_EXTI_Line) != RESET) {
		// If still in transmitting mode then finish transmission
		if (PTX) {

			mirf_CE_lo;                             // Deactivate transreceiver
			RX_POWERUP;                             // Power up in receiving mode
			mirf_CE_hi;                             // Listening for pakets
			PTX = 0;                                // Set to receiving mode

			// Reset status register for further interaction
			mirf_config_register(STATUS,(1<<TX_DS)|(1<<MAX_RT)); // Reset status register
		}
		EXTI_ClearITPendingBit(NRF_nIRQ_EXTI_Line);
	}
}
extern uint8_t mirf_data_ready() 
// Checks if data is available for reading
{
    if (PTX) return 0;
    uint8_t status;
    // Read MiRF status 
    mirf_CSN_lo;                                // Pull down chip select
    status = spi_fast_shift(NOP);               // Read status register
    mirf_CSN_hi;                                // Pull up chip select
    return status & (1<<RX_DR);
}

extern void mirf_get_data(uint8_t * data, uint8_t * len) 
// Reads mirf_PAYLOAD bytes into data array
{
    mirf_CSN_lo;                               // Pull down chip select
    spi_fast_shift( R_RX_PL_WID );             // Send cmd to read rx payload len
    spi_transfer_sync(len,len,1); 			   // Read payload len
	mirf_CSN_hi; 
	//uart_putw_dec(*len); uart_putc('\n');
	if(*len<=32) {
		mirf_CSN_lo;
	    spi_fast_shift( R_RX_PAYLOAD );            // Send cmd to read rx payload
	    spi_transfer_sync(data,data,*len); // Read payload
	    mirf_CSN_hi;                               // Pull up chip select
	} else {
	    mirf_CSN_lo;                    // Pull down chip select
	    spi_fast_shift( FLUSH_RX );     // Write cmd to flush tx fifo
	    mirf_CSN_hi;                    // Pull up chip select
	}
	mirf_config_register(STATUS,(1<<RX_DR));   // Reset status register
}

void mirf_config_register(uint8_t reg, uint8_t value)
// Clocks only one byte into the given MiRF register
{
    mirf_CSN_lo;
    spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
    spi_fast_shift(value);
    mirf_CSN_hi;
}

void mirf_read_register(uint8_t reg, uint8_t * value, uint8_t len)
// Reads an array of bytes from the given start position in the MiRF registers.
{
    mirf_CSN_lo;
    spi_fast_shift(R_REGISTER | (REGISTER_MASK & reg));
    spi_transfer_sync(value,value,len);
    mirf_CSN_hi;
}

void mirf_write_register(uint8_t reg, uint8_t * value, uint8_t len) 
// Writes an array of bytes into inte the MiRF registers.
{
    mirf_CSN_lo;
    spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
    spi_transmit_sync(value,len);
    mirf_CSN_hi;
}


void mirf_send(uint8_t * value, uint8_t len) 
// Sends a data package to the default address. Be sure to send the correct
// amount of bytes as configured as payload on the receiver.
{
    while (PTX) {}                  // Wait until last paket is send

    mirf_CE_lo;

    PTX = 1;                        // Set to transmitter mode
    TX_POWERUP;                     // Power up

	mirf_config_register(RX_PW_P0, len);
	    
    mirf_CSN_lo;                    // Pull down chip select
    spi_fast_shift( FLUSH_TX );     // Write cmd to flush tx fifo
    mirf_CSN_hi;                    // Pull up chip select
    
    mirf_CSN_lo;                    // Pull down chip select
    spi_fast_shift( W_TX_PAYLOAD ); // Write cmd to write payload
    spi_transmit_sync(value,len);   // Write payload
    mirf_CSN_hi;                    // Pull up chip select
    
    mirf_CE_hi;                     // Start transmission
}
