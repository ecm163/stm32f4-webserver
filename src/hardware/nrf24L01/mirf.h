/*
    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.

    $Id$
*/

#ifndef _MIRF_H_
#define _MIRF_H_

#include "main.h"
#include "nRF24L01.h"

// Mirf settings
#define mirf_CH         84
#define mirf_PAYLOAD    16
#define mirf_CONFIG     ( (1<<MASK_RX_DR) | (1<<EN_CRC) | (1<<CRCO) )

// Pin definitions for chip select and chip enabled of the MiRF module

/* SPI2  */
#define NRF_SPI                           SPI2
#define NRF_SPI_CLK                       RCC_APB1Periph_SPI2
#define NRF_SPI_CLK_INIT                  RCC_APB1PeriphClockCmd

/* SPI2 CLK PB.10 */
#define NRF_SPI_SCK_PIN                   GPIO_Pin_10
#define NRF_SPI_SCK_GPIO_PORT             GPIOB
#define NRF_SPI_SCK_GPIO_CLK              RCC_AHB1Periph_GPIOB
#define NRF_SPI_SCK_SOURCE                GPIO_PinSource10
#define NRF_SPI_SCK_AF                    GPIO_AF_SPI2

/* SPI2 MISO PC.2 */
#define NRF_SPI_MISO_PIN                  GPIO_Pin_2
#define NRF_SPI_MISO_GPIO_PORT            GPIOC
#define NRF_SPI_MISO_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define NRF_SPI_MISO_SOURCE               GPIO_PinSource2
#define NRF_SPI_MISO_AF                   GPIO_AF_SPI2

/* SPI2 MOSI PC.3 */
#define NRF_SPI_MOSI_PIN                  GPIO_Pin_3
#define NRF_SPI_MOSI_GPIO_PORT            GPIOC
#define NRF_SPI_MOSI_GPIO_CLK             RCC_AHB1Periph_GPIOC
#define NRF_SPI_MOSI_SOURCE               GPIO_PinSource3
#define NRF_SPI_MOSI_AF                   GPIO_AF_SPI2

/* SPI2 CS PC13 */
#define NRF_CSN_PIN                       GPIO_Pin_13
#define NRF_CSN_GPIO_PORT                 GPIOC
#define NRF_CSN_GPIO_CLK                  RCC_AHB1Periph_GPIOC

/* CE PC15 */
#define NRF_CE_PIN                        GPIO_Pin_15
#define NRF_CE_GPIO_PORT                  GPIOC
#define NRF_CE_GPIO_CLK                   RCC_AHB1Periph_GPIOC


/* nIRQ */
#define NRF_nIRQ_PIN                      GPIO_Pin_14
#define NRF_nIRQ_GPIO_PORT                GPIOC
#define NRF_nIRQ_GPIO_CLK                 RCC_AHB1Periph_GPIOC
#define NRF_nIRQ_EXTI_PORT				  EXTI_PortSourceGPIOC
#define NRF_nIRQ_EXTI_PIN				  EXTI_PinSource14
#define NRF_nIRQ_EXTI_Line				  EXTI_Line14
#define NRF_nIRQ_EXTI_IRQn				  EXTI15_10_IRQn

//the interrupt vector
#define NRF_ISR							void EXTI15_10_IRQHandler(void)


// Definitions for selecting and enabling MiRF module
#define mirf_CSN_hi     GPIO_SetBits(NRF_CSN_GPIO_PORT, NRF_CSN_PIN);
#define mirf_CSN_lo     GPIO_ResetBits(NRF_CSN_GPIO_PORT, NRF_CSN_PIN);
#define mirf_CE_hi      GPIO_SetBits(NRF_CE_GPIO_PORT, NRF_CE_PIN);
#define mirf_CE_lo      GPIO_ResetBits(NRF_CE_GPIO_PORT, NRF_CE_PIN);

// Public standart functions
extern void mirf_init();
extern void mirf_config();
extern void mirf_send(uint8_t * value, uint8_t len);
extern void mirf_set_RADDR(uint8_t * adr);
extern void mirf_set_TADDR(uint8_t * adr);
extern uint8_t mirf_data_ready();
extern void mirf_get_data(uint8_t * data, uint8_t * len);
extern void polling() ;

// Public extended functions
extern void mirf_config_register(uint8_t reg, uint8_t value);
extern void mirf_read_register(uint8_t reg, uint8_t * value, uint8_t len);
extern void mirf_write_register(uint8_t reg, uint8_t * value, uint8_t len);

#endif /* _MIRF_H_ */
